Google translate
	"Who is it?"
     •
    "Hello, we are from the Mandate of Heaven investigation force..."
     •
    "Destiny? Hmph, first the theft, then the robbery, is it the turn of the scammer this time?"
     •
    "Huh? No, we are really destiny..."
     •
    "I don't believe it... If Destiny still remembers Changkong City, whoever will get you people to do evil!"
     •
    "The pharmacy was also looted, and the house was smashed. The police in this place are unreliable. I managed to hide here, but I was still found...I've had enough, I've had enough..."
     •
    "Racked? Are you a victim of an attack?"
     •
    "Wait a minute, we are really the Valkyries of Destiny!"
     •
    "So what? It doesn't matter. Now I'm not afraid of anyone... I have nothing to lose anyway, haha!"
     •
    "The other party slammed the door shut."
 CN Text 
   "谁啊？"
    •
   "你好，我们是天命调查部队的……"
    •
   "天命？哼，先是盗窃，然后是抢劫，这回轮到诈骗了吗？"
    •
   "哎？不是的，我们真的是天命的人……"
    •
   "我才不信……天命要是还记得长空市，哪轮得到你们这些人为非作歹！"
    •
   "药房也被洗劫一空了，家也被砸了，这破地方的警察根本靠不住。好不容易躲到这里来，居然还能被找上门……我受够了，真受够了……"
    •
   "洗劫？你是袭击的受害者吗？"
    •
   "请等一下，我们真的是天命的女武神呀！"
    •
   "是又怎么样？都无所谓了。现在我可谁都不怕了……反正我也没什么可失去的了，哈哈！"
    •
   "对方直接用力关上了门。"