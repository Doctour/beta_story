Google translate
	"Hello—is anyone here?"
     •
    "..."
     •
    "Hello, who are you looking for?"
     •
    "Hello, kid. Is your adult at home?"
     •
    "Dad went to work."
     •
    "What about mom?"
     •
    "It's just me and dad at home."
     •
    "Ah... sorry..."
     •
    "No need to apologize. Sis, you're the police, aren't you?"
     •
    "Huh? Why do you think so?"
     •
    "I have been waiting for you. The book said that after calling the police, the police will come to the door to "collect evidence." So I've been waiting for you all day. "
     •
    "Could it be that the person who called the security maintenance station last night was..."
     •
    "It's me."
     •
    "So it is. We are not the police, but we are the companions of the police, from an organization called the Mandate of Heaven."
     •
    "Ah, I know the Mandate of Heaven. It's the organization that maintains world peace, right? It's also the police."
     •
    "...Maybe it can be said that. At least for now, we are maintaining the peace of Changkong City. Therefore, we hope to use everyone's strength."
     •
    "Can you tell us what you saw last night?"
     •
    "Of course, I have been practicing for this moment for a long time."
     •
    "Last night, as usual, I read a book while I waited for Dad to come home."
     •
    "Dad works in a building materials factory. Because many places in the city were attacked by bad guys, Dad patrolled with other uncles every night and came back very late."
     •
    "Usually, I'm stuck until about eleven o'clock in the evening before I start to get sleepy, so I've never had success waiting for him to come back..."
     •
    "Well, I get it, it happens every time I stay up late to study."
     •
    "But last night was different. Just when I was about to fall asleep, I was woken up by a 'buzzing' sound. "
     •
    "I looked out the window and saw a group of cloaked men coming down the street. They jumped out of a truck and walked past my house one by one to a warehouse at the end of the street, very lightly. ."
     •
    "I saw them go to the warehouse door, but instead of taking the key and knocking on the door, they just opened a big hole in the door with a strange tool."
     •
    "I thought, that must be the group of bad guys Dad was talking about. So I called the police right away!"
     •
    "However, they didn't believe what I said at first. After I explained it for a long time, they agreed to send someone over to take a look."
     •
    "Is it because the caller is a child, so they are careless?"
     •
    "How can this be so unreliable!"
     •
    "Then what happened?"
     •
    "Later... before the police came, those people left, and the outside became quiet. As a result... I accidentally fell asleep."
     •
    "When I woke up again, it was already daytime. Police sisters, have you caught those people?"
     •
    "We'll catch them soon, don't worry too much."
     •
    "Really?"
     •
    "Of course. You see, haven't we already launched an investigation here?"
     •
    "(This child is an eyewitness to "The Faceless One"...Guide her here, maybe she can ask more important information...)"
     •
    "About the warehouse at the end of the street..."
     •
    "You said they went to the warehouse at the end of the street. What kind of things are usually stored there?"
     •
    "Well, I don't know either, I just know that an uncle lives there."
     •
    "He never interacted with people, so people didn't know what he was doing there. The old man across the street was always complaining about it."
     •
    "...So it's the warehouse that the old uncle mentioned."
     •
    "Ms. Police, do you have anything else to ask?"
     •
    "About the sound that woke you up..."
     •
    "What do you think is that "buzzing" sound that wakes you up? "
     •
    "I don't know, but I can hear that sound from time to time. Although it's not very loud, it keeps ringing intermittently, like mosquitoes, and it's still very noisy."
     •
    "In the past, if there was such an uncomfortable movement, the drinking uncle next door would definitely curse loudly. However, last night his side was very quiet."
     •
    "Have you ever heard a sound like this before?"
     •
    "Well……"
     •
    "...Speaking of which, I seem to have heard similar voices when I visited my father's factory. It's just that I complained to my father at that time, and he still didn't believe me."
     •
    "(A voice that Dad can't hear? Could it be Dr. Einstein said...)"
     •
    "By the way, Sora, do you still have Mark Rabbit with you?"
     •
    "Huh? Mark Rabbit? I do have it all the time, what's wrong?"
     •
    "Can you please take it out?"
     •
    "Even though he didn't understand Seele's intentions, Sora took out Mark Rabbit from under the cloak."
     •
    "Ah, yes! The sound is a bit similar to this, and the feeling of "buzzing" is the same! "
     •
    "Is she referring to... the sound made by Mark Rabbit?"
     •
    "Not the Mark Rabbit itself."
     •
    "I think what she heard was the high-frequency vibration of the parts when the machine was running. Dr. Einstein said that the electromagnetic coil in the old picture tube would make a similar sound."
     •
    "Ah. I remembered, when I was a child, some antique machines in my family's collection would also make this sound!"
     •
    "High-frequency sounds are difficult for the human ear to capture, and most adults can't hear such sounds. This child should be of the type that is more sensitive to high-frequency sounds, so he can hear so clearly."
     •
    "Some of the parts on Mark Rabbit's body are also... more classic in style, so that's why they make similar sounds."
     •
    "Um……"
     •
    "(However, not all the equipment used by "The Faceless"...why do they have this sound?)"
     •
    "Ms. Police, do you have anything else to ask?"
     •
    "About the man in the cloak..."
     •
    "Are there any features about those people other than the cloaks they wear?"
     •
    "Let's think about it... by the way, there is something very scary!"
     •
    "Scary?"
     •
    "Well, it's like this. After the cloaked men made holes in the door, a man who looked like a leader directed them into the house one by one, and he stood by the truck and waited."
     •
    "That's when I realized that the leader was standing just under the rearview mirror of the truck. From my angle, through that rearview mirror, I could see the face under the cloak."
     •
    "So, I just took a look..."
     •
    "Well...then what...?"
     •
    "I found out that under the cloak... there is no face at all!"
     •
    "?!"
     •
    "No face... what does that mean?"
     •
    "It's just pitch black, you can't see his face at all! You can't even see his eyes!"
     •
    "..."
     •
    "And then? Did you see anything else?"
     •
    "No more. As soon as I took a look, he suddenly raised his hand and punched the rearview mirror. The mirror shattered and I couldn't see anything."
     •
    "He...wouldn't he notice that you were peeping?"
     •
    "Probably not, because he didn't look back after smashing the mirror, but squatted on the ground with his head in his arms, as if he wasn't feeling well."
     •
    "This behavior sounds even weirder...won't it make me encounter a "ghost" again? "
     •
    "again?"
     •
    "Take it easy, Suzanne, there must be a reason why it's like this... let's write it down first."
     •
    "But Xi'er, your complexion is also very bad..."
     •
    "..."
     •
    "Ms. Police, do you have anything else to ask?"
     •
    "There's nothing left to ask."
     •
    "(Maybe there is still missing information, let's inquire again...)"
     •
    "This information is already sufficient, thank you very much for your help!"
     •
    "I assure you, we will definitely solve the incident caused by "The Faceless Man", and then your father will be able to go home early to accompany you. "
     •
    "Well! Thank you for your hard work, police sisters!"
     •
    "The crowd waved goodbye to the girl."
     •
    "Finally asked about some information related to the case."
     •
    "Sister Xier seems to be thinking all the time, did you notice something unusual?"
     •
    "Hmm... I'm not sure. I always feel that there are some subtle contradictions in the things I just asked, but I can't figure it out at once..."
     •
    "In short, let's go to the scene to investigate next. Based on everyone's testimony, the crime scene should be in the north, at the end of this street."
 CN Text 
   "你好——请问有人吗？"
    •
   "……"
    •
   "你们好呀，请问是来找谁的？"
    •
   "小朋友，你好呀。请问你家大人在家吗？"
    •
   "爸爸去上工了。"
    •
   "那妈妈呢？"
    •
   "家里只有我和爸爸。"
    •
   "啊……不好意思……"
    •
   "不用道歉呀。姐姐，你们是警察，对不对？"
    •
   "咦？为什么这么想？"
    •
   "我一直在等着你们呢。书上说，报警之后，就会有警察上门「收集证据」。所以我这一整天都在等你们。"
    •
   "难道，昨天晚上给治安维护所打电话的就是……"
    •
   "就是我呀。"
    •
   "原来是这样啊。我们不是警察，但我们是警察的同伴，来自一个叫做天命的组织。"
    •
   "啊，我知道天命。就是那个维护世界和平的组织对吧？那不就也是警察嘛。"
    •
   "……或许可以这么说吧。至少现在，我们正在维护长空市的和平。所以，我们希望能借助每个人的力量。"
    •
   "可以把你昨天晚上看到的事情告诉我们吗？"
    •
   "当然可以了，为了这一刻，我已经练习了好久了。"
    •
   "昨天晚上，我像往常一样，边看书边等爸爸回家。"
    •
   "爸爸在建材厂工作，因为城里很多地方都被坏人团体袭击了，所以爸爸每天晚上都和其他叔叔一起巡逻，回来得非常晚。"
    •
   "一般来说，我坚持到晚上十一点左右就会开始犯困了，所以从来没有成功等到他回来过……"
    •
   "嗯，我懂的，每次我熬夜念书也都会变成这样。"
    •
   "但是昨晚不一样。就在我快要睡着的时候，突然听到一阵「嗡嗡嗡」的声音，把我吵醒了。"
    •
   "我从窗口探头往外看，发现街上来了一伙披着斗篷的人。他们从一辆卡车上跳下来，挨个从我家门口走过，直奔街尾的一间仓库去了，动作非常轻。"
    •
   "我看见他们走到仓库门前，却没有拿钥匙，也不敲门，而是直接用一个奇怪的工具在门上开了个大洞。"
    •
   "我想，那一定就是爸爸说的坏人团体吧。所以我马上给警察打了电话！"
    •
   "可是，他们一开始不相信我说的话。我解释了半天，他们才答应派人过来看看。"
    •
   "是因为报警人是小孩子，他们就大意了吗？"
    •
   "怎么能这样，也太不靠谱了！"
    •
   "那后来发生了什么？"
    •
   "后来……在警察过来之前，那些人就离开了，外面也安静下来了。结果……我就一不小心睡着了。"
    •
   "再醒来的时候，就已经是白天了。警察姐姐们，你们抓到那些人了吗？"
    •
   "我们很快会抓到他们的，你不用太担心哦。"
    •
   "真的吗？"
    •
   "当然啦。你看，我们不是已经在这里展开调查了吗？"
    •
   "（这孩子是「无貌者」的目击者……在这里引导她一下，或许能问出更多重要的情报……）"
    •
   "关于街尾的仓库……"
    •
   "你说他们朝着街尾的仓库去了，那里平常存放着什么东西呀？"
    •
   "唔，我也不清楚，只知道有个叔叔住在那里面。"
    •
   "他从来不和人打交道，所以大家也不知道他在那里做些什么。街对面的老伯伯还总是跟大家抱怨这件事呢。"
    •
   "……原来是那个老伯伯提到过的仓库啊。"
    •
   "警察姐姐还有什么要问的吗？"
    •
   "关于吵醒你的声音……"
    •
   "那个吵醒你的「嗡嗡嗡」的声音，你觉得是什么呀？"
    •
   "我也不知道，只是时不时能听到那个声音。虽然不是很大声，但断断续续地一直响着，像蚊子一样，还是很吵。"
    •
   "以往如果有这种让人不舒服的动静，隔壁爱喝酒的叔叔肯定要大声骂人了。不过，昨天晚上他那边倒是非常安静。"
    •
   "你以前有听到过类似的声音吗？"
    •
   "唔……"
    •
   "……这么说起来，去爸爸的工厂参观的时候，我好像也听到过类似的声音。只是那时候和爸爸抱怨，他还不相信我。"
    •
   "（爸爸听不到的声音？该不会是爱因斯坦博士说过的……）"
    •
   "对了，小空，你还随身带着马克兔吗？"
    •
   "哎？马克兔吗？我确实还一直带着它，怎么了？"
    •
   "能请你把它拿出来吗？"
    •
   "虽然不明白希儿的用意，小空还是从斗篷下拿出了马克兔。"
    •
   "啊，对！跟这个声音有点像，「嗡嗡嗡」的感觉是一样的！"
    •
   "难道她指的是……马克兔发出的声音吗？"
    •
   "倒不是马克兔本身啦。"
    •
   "我想，她听到的，应该是机器运行时，部件发出的高频振动声吧。爱因斯坦博士说，老式显像管里的电磁线圈就会发出类似的声音。"
    •
   "啊。我想起来了，小时候，家里收藏的一些古董机械也会发出这种声音！"
    •
   "高频率的声音很难被人耳捕捉到，大部分成年人都听不见这种声音。这孩子应该属于对高频声音比较敏感的类型，才能听得如此清晰。"
    •
   "马克兔身上的一些部件也……款式比较经典，所以才会发出类似的声音吧。"
    •
   "嗯……"
    •
   "（但是，「无貌者」使用的设备不都是……为什么也会有这种声音？）"
    •
   "警察姐姐还有什么要问的吗？"
    •
   "关于披着斗篷的人……"
    •
   "除了披着斗篷以外，那些人身上还有什么特征吗？"
    •
   "我想想……对了，有件事情非常吓人！"
    •
   "吓人？"
    •
   "嗯，是这样的，那些披着斗篷的人在门上开洞以后，有一个看上去像是头领的人指挥他们一个一个进屋，他自己就站在卡车边上等着。"
    •
   "这时候我发现，那个头领站的位置，刚好就在卡车的后视镜下面。从我的角度，透过那面后视镜，可以看见斗篷下面的脸。"
    •
   "所以，我就看了一眼……"
    •
   "然……然后呢……？"
    •
   "我发现斗篷下面……根本就没有脸！"
    •
   "？！"
    •
   "没有脸……那是什么意思？"
    •
   "就是黑漆漆的一片，完全看不见他的脸！连眼睛都看不出来！"
    •
   "……"
    •
   "然后呢？你还有看到其他什么吗？"
    •
   "没有了。我刚看了一眼，他就突然举起手，一拳打在了那面后视镜上。镜子碎掉了，我就什么也看不见了。"
    •
   "他……该不会发现你在偷看了？"
    •
   "应该没有吧，因为他砸碎镜子后没有回头，而是抱着脑袋蹲在地上，好像不太舒服的样子。"
    •
   "这举动听上去更诡异了……该不会又让我撞见「幽灵」了吧？"
    •
   "又？"
    •
   "别紧张，苏莎娜，会变成这样一定是有原因的……我们先把这件事记下来吧。"
    •
   "可是希儿，你的脸色也很不妙呢……"
    •
   "……"
    •
   "警察姐姐还有什么要问的吗？"
    •
   "没有什么要问的了。"
    •
   "（也许还有遗漏的情报，还是再打听打听吧……）"
    •
   "这些情报已经很充分了，非常感谢你的帮助！"
    •
   "我向你保证，我们一定会解决「无貌者」引发的事件，到时候你的爸爸也能早早回家陪你哦。"
    •
   "嗯！辛苦警察姐姐们了！"
    •
   "众人与女孩挥手告别。"
    •
   "总算问到一些跟案件有关的情报了。"
    •
   "希儿姐姐似乎一直在思考的样子，是注意到了什么不寻常的地方吗？"
    •
   "嗯……我也不是很确定。总觉得刚刚问到的事情里存在一些细微的矛盾，但是又没办法一下子想明白……"
    •
   "总之，接下来我们就去现场勘察一下吧。结合大家的证词，案发现场应该就在北边，这条街的尽头。"
 