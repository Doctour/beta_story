Google translate
	"It's really strange. We didn't even see a single person on the way except for the armored armor."
     •
    "And those mechas... no matter the model or the equipment, no matter how you look at it, they are all armed by the "Faceless". "
     •
    "It's very likely that this place... has been taken over by the Faceless Ones. "
     •
    "Are we late? Oh shit, that Gray Snake got the bionic parts he wanted and ran away?"
     •
    "Don't worry, I think he hasn't succeeded yet—otherwise, he has no reason to keep these mechas and surround Xincheng Medicine from inside to outside."
     •
    "He should be somewhere in this building... Let's find out if there is any way to the warehouse or research room that has traces of him."
     •
    "good!"
 CN Text 
   "果然很奇怪，这一路上除了武装机甲，我们连一个人都没有看到。"
    •
   "而且那些机甲……无论是型号还是装备，怎么看都是「无貌者」的武装。"
    •
   "这里……很有可能已经被「无貌者」占领了。"
    •
   "我们来晚了？糟了，不会那个灰蛇已经拿到想要的仿生部件，逃之夭夭了吧？"
    •
   "先别着急，我觉得他应该还没有得手——不然，他没有理由留下这些机甲，把新城医药里里外外包围起来。"
    •
   "他应该就在这栋大楼的某处……我们快去找找，有没有通往仓库或者研究室的路有他留下的痕迹。"
    •
   "好！"