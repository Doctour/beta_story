Google translate
	"The open space at the four corners of the warehouse, which was obviously used to store goods, has now been swept away, and it looks abnormally empty."
     •
    "(Um?)"
     •
    "Xiao Kong, can you come over and take a look?"
     •
    "What's the matter, Sister Seele?"
     •
    "Seele picked up a crushed cardboard box and carefully spread out the side with the logo on it."
     •
    "Is this trademark from a company in Changkong City?"
     •
    "Hmm... I always feel familiar, but I can't remember clearly..."
     •
    "Mark Rabbit, can you recall?"
     •
    "..."
     •
    "Mark Rabbit scanned the logo."
     •
    "Give us a little time, Sister Xier. Mark Rabbit should be able to recall."
     •
    "Well, please, Sora."
 CN Text 
   "仓库四角的空地，原本显然是用来堆积货物的，如今被一扫而空，反而显得异常空荡。"
    •
   "（嗯？）"
    •
   "小空，能过来帮忙看一下吗？"
    •
   "怎么了，希儿姐姐？"
    •
   "希儿捡起一个被压瘪的纸箱，小心地将印有商标的一面摊开。"
    •
   "这个商标，是来自长空市的企业吗？"
    •
   "唔……总觉得有些眼熟，但是又记不太清楚了……"
    •
   "马克兔，你能回忆起来吗？"
    •
   "……"
    •
   "马克兔对着商标扫描了一番。"
    •
   "给我们一点时间吧，希儿姐姐。马克兔应该能回想起来的。"
    •
   "嗯，拜托了，小空。"