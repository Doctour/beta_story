V2 new info below this line
################################################################

Next group of chapters will be labeled as 
	"Across the Last Day"
	致以无瑕之人
	(this is like how Ch32 to 34 were part of the group "New Journey")

Chapter 36 Name I think
	Desolate city detection
	荒城侦破

Outfit Name of wahtever
	Let's have some music!
	来点音乐！

Rita Birthday Card
	Sir Captain, how do you do.
	The sun is just right these days, and you can smell the sweet fragrance of flowers in the wind even when you open the window. It is the most suitable time to dry the bedding. You always take care of me not to push too hard, but the fluffy bedding and soft pillows can bring you a perfect night's sleep--for a maid, this is a very meaningful thing, so please stop declined.
	Captain, you clearly said you should exercise well, work and rest regularly, and take advantage of the good weather to go out for a walk, but you have been staying in the room and working at your desk. I can't tell when your affairs have become so burdensome. So, while you are taking a nap on the table, I have finished some preliminary work, and maybe I can buy you a moment of leisure time.
	In a while I'm going to go to the flower shop we visited last time to buy some flowers for decoration. Since the captain has no time for outings, let Rita bring the spring to the room. Fragrant roses, light sweet peas, chamomile and freesia... the smell of flowers while working, hoping to dilute your tiredness.
	By the time you read this letter, I should have returned from the flower shop. If the captain is interested, how about arranging flowers with me? I have prepared flower tea and desserts suitable for spring, and I look forward to enjoying them with you.

	舰长大人，贵安。
	近日阳光正好，推开窗也能闻到风中清甜的花香，在这个时候晾晒寝具最为合适。您总是关照我不要太过勉强，但是蓬松的被褥和柔软的枕头，可以为您来带一场完美的睡眠——对于女仆而言，这是一件很有意义的事情，所以请不要再推辞了。
	舰长大人啊，明明说过要好好锻炼，规律作息，趁着好天气出去走走，可是却一直呆在房间里伏案工作呢。也说不清是从什么时候起，您的事务就变得这样繁重了。所以，在您趴在桌上小憩时，我已经把一些初步的工作完成了，或许可以为您争取到片刻的闲暇时光。
	一会我准备去我们上次去过的那家花店，买一些鲜花回来作为装饰。既然舰长无暇踏青，那么就由丽塔把春天带到房间里来吧。馥郁的玫瑰、轻灵的香豌豆、还有洋甘菊和小苍兰……工作时嗅到花香，希望能冲淡您的倦意。
	当您看到这封信时，我应该已经从花店回来了。如果舰长有兴趣，不妨陪我一起插花，如何？我准备了适合春日的花茶和甜点，期待与您一同享用。

Rita Birthday Item
	Rose Tea
	The hot scented tea placed next to the letterhead is served in a teacup carefully selected by Rita. After using it, you can gain 30 stamina
	放在信笺旁的热花茶，盛放在丽塔精心挑选的茶杯中，使用后可以获得30体力

idk if this was announced yet, but some kind of box fromsomething (mayeb bp?) with outfits
	You can choose to exchange 1 item among: Clothing - Switch Electric, Clothing - Pure Day Dome, Clothing - Midnight Martini, Clothing - Waiting for get out of class, 8000 Star Stones.
	可在：服装-掣电机动、服装-纯白天穹、服装-午夜马天尼、服装-等待下课、8000星石中选择兑换1项。









V1 below this line
################################################################

Edit for this, its apparently Nagazora so we're going back
	Would be nice to know if 长空市了 is a city/location weve been to before

Random text related to Susannah
	Susanna's 20-year-old fan
	苏莎娜的二十年老粉 (text colour is <color=#fedf4c>) 

Idk what this refers too
	Angry Night Owl
	「愤怒夜猫侠号」

Quote
	"What gives life to a story is precisely what it records"
	赋予故事生命的，恰是被其记录着的一切



Abyss weather I think

	"The enemy can only be defeated in the collapsed state"
	"When the enemy receives less than 800 damage, it will partially reduce the damage, but when it receives more than 800 damage, it will increase the damage"
    "Enemies can only be defeated while bleeding"
    "敌人只有在坍缩态才能被击败"
    "敌人受到800点以下伤害时具有部分减伤，但受到800点以上的伤害时具有增伤"
    "敌人只有在流血状态下才能被击败"
   
   
   
Chapter mission numbers
   "36-1"
   "36-2"
   "36-3"
   "36-4"
   "36-5"
   "36-6"
   "36-7"
   "36-8"
   "36-9"
   "36-10"
   "36-11"
   "36-12"
   "36-13"
   "36-14"
 
Effect of new Elysian Realm item buff thingy i forget the name
	Increases the total damage of special moves and explosive attacks by #1[f1]% (only valid in "Elysian Realm")
	必杀技和爆发攻击成的全伤害提高#1[f1]%（仅在「往世乐土」生效）

Boss Kevin is called
	'Salvation' Kevin
	「救世」凯文   
  
Suply of Some sort wiht the "Chasing the Light"/「逐光舞步」 only mentioned here
	"Chasing Light" Supply Protection Instructions
	"Chasing Light" Supply Probability Announcement
	"Chasing Light" Supply Log
  ◆Explanation that 120 supplies must be given. In the supply of "Dance of the Light", the captain will definitely get a piece of UP equipment that has not been obtained for every 30 supplies, that is, at most 120 supplies must get all UP equipment. 
  This mechanism only takes effect once during this supply period. The captain can check the maximum remaining supply times for the current UP equipment on the supply interface. 
  ◆Inheritance rules for supply times This mechanism only applies to the supply of "Dance of the Light" in the current period, it is not shared with other supplies, and will not be inherited to other supplies.

	「逐光舞步」补给保护说明
	「逐光舞步」补给概率公示
	「逐光舞步」补给记录
 ◆120次补给必出的说明舰长在「逐光舞步」补给中，每进行30次补给必定获得一件未获得的UP装备，即至多120次补给必定获得全部UP装备。 该机制在本次补给期间仅生效 1次。
	舰长可以在补给界面查看获得当期UP装备的最大剩余补给次数。
 ◆补给次数的继承规则该机制仅作用于当期「逐光舞步」补给，不与其它补给共用，且不会继承到其它补给中。
   
   
Elysian Realm related skills/effects

	"When attacking <color=#ff5e41>hitting</color> the enemy will drop <color=#ff5e41>6 pieces</color> (triggered at most once every 0.5 seconds), the enemy is defeated by <color=#ff5e41>< /color> will drop <color=#ff5e41>5 pieces</color>. The release of active skills will consume <color=#ff5e41>100</color> parts and generate <color=#ff5e41>10 seconds black hole </color> Causes continuous damage, and the damage received by enemies <color=#ff5e41> affected by the black hole is increased by 30%</color>\\n<color=#FFAF37FF>The whole team of Valkyrie attack +100</color>"
	"On the basis of <color=#ff5e41>enhanced skills 1-1</color>, an additional effect is added: when the active skill is released, the character will attack and hit the enemy within 10 seconds, and a small bomb will be placed on it and explode immediately causing a <color= #ff5e41>Area damage</color>, can be placed once every 3 seconds\\n<color=#FFAF37FF>All Valkyrie attack +100</color>"
	"On the basis of <color=#ff5e41>enhanced skills 1-1</color> and <color=#ff5e41>enhanced skills 1-2</color>, an additional effect: release active skills, trigger an explosion and cause a <color= #ff5e41>Full area damage</color>\\n<color=#FFAF37FF>Valkyrie attack of the whole team +100</color>"
    "If the enemy receives <color=#ff5e41>weapon skill damage</color>, it will enter the <color=#ff5e41>ignited state</color>\\nIn the ignited state, a ring of fire that causes continuous fire damage will be formed around the enemy\\ n<color=#ff5e41>combo</color> every time it reaches <color=#ff5e41>50</color>, the enemy will enter the <color=#ff5e41>bleeding state</color>\\nIn the bleeding state, gain the initiative Skill, use it to generate a black hole\\n<color=#FFAF37FF>Valkyrie attack of the whole team +100</color>"
    "The enemy receives <color=#ff5e41>Nirvana damage</color>, and will enter the <color=#ff5e41>stunned state</color>\\nIn the stunned state, the enemy's full damage is increased\\nThe enemy is <color=#ff5e41>Branch or charge skill damage</color>, will enter <color=#ff5e41>paralyzed state</color>\\nIn the paralyzed state, a lightning chain will be generated between the enemies and cause lightning damage\\n The Valkyrie triggers the <color=#ff5e41>Extreme Dodge Skill</color>, and the enemy enters the <color=#ff5e41>Freezing State</color>\\nIn the freezing state, attacking the enemy will cause freezing damage and splash effect\\ n<color=#FFAF37FF>All Valkyrie attack +100</color>"
	"Each time the enemy enters a <color=#ff5e41>abnormal state</color>, the <color=#ff5e41>physical and all-element damage it receives will be increased</color>\\n<color=#FFAF37FF>All women in the team Valkyrie attack +100</color>"
  CN Text 
    "攻击<color=#ff5e41>命中</color>敌人时会掉落<color=#ff5e41>6枚零件</color>（每0.5秒最多触发一次），敌人被<color=#ff5e41>击败</color>时会掉落<color=#ff5e41>5枚</color>。释放主动技能会消耗<color=#ff5e41>100枚</color>零件，同时生成<color=#ff5e41>10秒黑洞</color>造成持续伤害，受黑洞影响的敌人<color=#ff5e41>受到的伤害提升30%</color>\\n<color=#FFAF37FF>全队女武神攻击+100</color>"
    "在<color=#ff5e41>强化技能1-1</color>的基础上追加效果：释放主动技能会角色在10秒内攻击命中敌人，会给其安放小型炸弹并立即爆炸造成一次<color=#ff5e41>范围伤害</color>，每3秒可安放一次\\n<color=#FFAF37FF>全队女武神攻击+100</color>"
    "在<color=#ff5e41>强化技能1-1</color>和<color=#ff5e41>强化技能1-2</color>的基础上追加效果：释放主动技能，引发爆炸造成一次<color=#ff5e41>全场范围伤害</color>\\n<color=#FFAF37FF>全队女武神攻击+100</color>"
    "敌人受到<color=#ff5e41>武器技伤害</color>，会进入<color=#ff5e41>点燃状态</color>\\n点燃状态下，敌人周围生成造成持续火焰伤害的火圈\\n<color=#ff5e41>连击</color>每达到<color=#ff5e41>50</color>，敌人进入<color=#ff5e41>流血状态</color>\\n流血状态下，获得主动技能，使用后生成黑洞\\n<color=#FFAF37FF>全队女武神攻击+100</color>"
    "敌人受到<color=#ff5e41>必杀技伤害</color>，会进入<color=#ff5e41>眩晕状态</color>\\n眩晕状态下，敌人受到的全伤害提升\\n敌人受到<color=#ff5e41>分支或蓄力技伤害</color>，会进入<color=#ff5e41>麻痹状态</color>\\n麻痹状态下，敌人间生成闪电链并造成雷电伤害\\n女武神触发<color=#ff5e41>极限闪避技能</color>，敌人进入<color=#ff5e41>冰冻状态</color>\\n冰冻状态下，攻击敌人会造成冰冻伤害及溅射效果\\n<color=#FFAF37FF>全队女武神攻击+100</color>"
    "敌人每进入一种<color=#ff5e41>异常状态</color>，其受到的<color=#ff5e41>物理和全元素伤害提升</color>\\n<color=#FFAF37FF>全队女武神攻击+100</color>"
	
	
Elysian realm buffs/debbufs I think

	"Enemies' resistance to stun, freeze, and paralysis is increased, store purchases and upgrade costs are all 20% off (only superimposed with the discount effect of the destined crossroads), at the end of each layer of battle, the HP of non-imaginary enemies increases by 2.5%, and the HP of imaginary enemies HP increased by 1%"
    "Enemies get 10% full damage reduction, each time they receive damage from Valkyrie's nirvana, non-leader enemies reduce 5% full damage reduction, leader enemies reduce 2% full damage reduction, and last until the end of this layer (this effect is an independent addition)"
    "Enemies get 10% full damage reduction, each time they receive damage from Valkyrie's nirvana, non-leader enemies reduce 5% full damage reduction, leader enemies reduce 2% full damage reduction, and last until the end of this layer (this effect is an independent addition)"
    "Enemies get 30% full damage reduction, each time they receive damage from Valkyrie's nirvana, non-leader enemies reduce 3% full damage reduction, leader enemies reduce 1.5% full damage reduction, and last until the end of this layer (this effect is an independent addition)"
    "Every time the enemy enters one of the states of paralysis, ignition, bleeding, floating, and stun, the total damage received increases by 2% for 10 seconds. Each layer is timed separately, and the duration is refreshed by repeated triggers."
    "When obtaining the ordinary engraving of spiral and floating life, its level +2"
    "When Valkyrie deals physical damage, the lightning damage dealt is increased by 50% for 10 seconds, repeated triggers refresh the duration"
    "The enemy gets 5% full damage reduction, and the weapon type is Taidao, double guns, heavy artillery and gauntlets. It will be invalid when Valkyrie is on the field, including Valkyrie. (This effect is an independent bonus)"
    "The enemy gets 30% full damage reduction, and the weapon type is Taidao, double guns, heavy artillery and gauntlet Valkyrie are on the field, including the Valkyrie who assists the battle. (This effect is an independent bonus)"
    "The enemy gets 5% full damage reduction, and enters the state of floating, ignited, paralyzed, bleeding, and time-space deceleration will reduce 1% of the full damage reduction (each state is only calculated once), and lasts until the end of this layer (this effect is independent addition)"
    "The enemy gets 30% full damage reduction, and entering the floating, ignited, paralyzed, bleeding, time-space deceleration state will reduce 6% of the full damage reduction (each state is only calculated once), and lasts until the end of this layer (this effect is independent addition)"
    "After releasing a branch attack and hitting an enemy, the enemy's total damage will be increased by 15% for 10 seconds, and the duration will be refreshed by repeated triggers"
    "When you get the normal engraving of Fanxing and Tianhui, its level +2"
    "Enemies' resistance to stun, freeze, and paralysis is increased, the level of the first 5 ordinary engravings obtained is +1, the enemy's basic health value is increased by 5%, after each layer of battle, the non-imaginary enemy's health value is increased by 0.2%, and the imaginary enemy's health value is increased by 0.2%. Increase by 0.1%"
    "Enemies' resistance to stun, freeze, and paralysis is increased, the level of the first 5 ordinary engravings obtained is +1, the enemy's basic health value is increased by 5%, after each layer of battle, the non-imaginary enemy's health value is increased by 2%, and the imaginary enemy's health value is increased by 2%. Increased by 0.8%"
    "Enemies' resistance to stun, freeze, and paralysis is increased, and the basic engravings purchased in the store are reduced by 30%, but the cost of engraving upgrades is increased by 30%, and the cost of purchasing increased engravings is increased by 30%. The enemy's basic health is increased by 5%, and each layer of battle ends Afterwards, the HP of non-imaginary enemies increases by 0.2%, and that of imaginary enemies increases by 0.1%"
    "Enemies' resistance to stun, freeze, and paralysis is increased, and the basic engraving purchased in the store is reduced by 30%, but the engraving upgrade cost is increased by 30%, and the cost of purchasing an increase engraving is increased by 30%. The enemy's basic health value is increased by 5%, and each layer of battle ends Afterwards, the HP of non-imaginary enemies increases by 2%, and that of imaginary enemies increases by 0.8%"
 CN Text 
   "敌人获得10%的全伤害减免，每次受到出战女武神必杀技伤害，非首领敌人减少5%的全伤害减免，首领敌人减少2%的全伤害减免，持续到本层结束（该效果为独立加成）"
   "敌人获得30%的全伤害减免，每次受到出战女武神必杀技伤害，非首领敌人减少3%的全伤害减免，首领敌人减少1.5%的全伤害减免，持续到本层结束（该效果为独立加成）"
   "敌人每进入麻痹、点燃、流血、浮空、眩晕其中一种状态时，受到的全伤害提升2%，持续10秒，每层单独计时，重复触发刷新持续时间"
   "获得螺旋和浮生的普通刻印时，其等级+2"
   "出战女武神造成物理伤害时，造成的雷电伤害提高50%，持续10秒，重复触发刷新持续时间"
   "敌人获得5%全伤害减免，武器类型为太刀，双枪，重炮和拳套的女武神在场时失效，包括助战女武神。（该效果为独立加成）"
   "敌人获得30%全伤害减免，武器类型为太刀，双枪，重炮和拳套的女武神在场时失效，包括助战女武神。（该效果为独立加成）"
   "敌人获得5%的全伤害减免，进入浮空、点燃、麻痹、流血、时空减速状态会降低1%的全伤害减免（每种状态只计算一次），持续到本层结束（该效果为独立加成）"
   "敌人获得30%的全伤害减免，进入浮空、点燃、麻痹、流血、时空减速状态会降低6%的全伤害减免（每种状态只计算一次），持续到本层结束（该效果为独立加成）"
   "释放分支攻击命中敌人后，该敌人受到的全伤害提高15%，持续10秒，重复触发刷新持续时间"
   "获得繁星和天慧的普通刻印时，其等级+2"
   "敌人的眩晕、冻结、麻痹抗性增加，获得的前5个普通刻印其等级+1，敌人基础生命值提高5%，每层战斗结束后非虚数敌人生命值提高0.2%，虚数敌人生命值提高0.1%"
   "敌人的眩晕、冻结、麻痹抗性增加，获得的前5个普通刻印其等级+1，敌人基础生命值提高5%，每层战斗结束后非虚数敌人生命值提高2%，虚数敌人生命值提高0.8%"
   "敌人的眩晕、冻结、麻痹抗性增加，商店中购买基础刻印降低30%，但刻印升级费用提高30%，购买增幅刻印的花费提高30%。敌人基础生命值提高5%，每层战斗结束后非虚数敌人生命值提高0.2%，虚数敌人提高0.1%"
   "敌人的眩晕、冻结、麻痹抗性增加，商店中购买基础刻印降低30%，但刻印升级费用提高30%，购买增幅刻印的花费提高30%。敌人基础生命值提高5%，每层战斗结束后非虚数敌人生命值提高2%，虚数敌人提高0.8%"
   
   
Some Shadow Knight Signets
  	"The full damage caused by the branch attack is increased. The last blow of the branch attack [Inch Strength·Splitting Wave], [Inch Strength·Instant Dust], [Death Force·Wakening Moon] will cause an additional lightning elemental damage, which is regarded as branch damage and ultimate skill. harm"
    "Branch attack [Zhijin · Cracking the Sky] will cause additional thunder and lightning element damage, which is regarded as branch and ultimate skill damage. In [lunar eclipse form], after the extreme dodge, you can actively trigger the extreme dodge skill [Wuque], and [Zhijin · Hitting the enemy can reduce the cooldown time of [Wuque], and the cooldown time is 0.6 seconds"
    "The lightning elemental damage caused by the branch attack [Death Force·Splitting Sky] is increased, and you can hit more in a row, and the last hit will cause an additional lightning element damage. This damage is regarded as branch damage and ultimate skill damage. The last hit is a heavy attack"
    "In the burst state [lunar eclipse form], when the energy value is sufficient, the nirvana [crazy dragon] can be triggered again, and the damage of the nirvana attack and the attack of the burst state attack will increase."
    "The full damage caused by the branch attack [Deadly Moon] is increased, and the bleeding accumulation value is caused, and the character's full damage to the enemy in the bleeding state is increased"
  
 CN Text 
   "分支攻击造成的全伤害提高，分支攻击【寸劲·裂波】【寸劲·瞬尘】【死劲·残月】最后一击，额外造成一次雷电元素伤害，该伤害视为分支伤害和必杀技伤害" 
   "分支攻击【死劲·裂空】连打额外造成雷电元素伤害，视为分支和必杀技伤害。【月食形态】下，极限闪避后可以主动触发极限闪避技能【乌雀】，且【死劲·裂空】命中敌人可以降低【乌雀】冷却时间，冷却时间0.6秒"
   "分支攻击【死劲·裂空】造成的雷电元素伤害提高，连打可以额外多打，且最后一击额外造成一次雷电元素伤害，该伤害视为分支伤害和必杀技伤害，【死劲·裂空】最后一击为重攻击"
   "爆发状态【月食形态】下，能量值充足时，可以再次触发必杀技【狂龙】，必杀技攻击和爆发状态攻击雷电元素伤害提高"
   "分支攻击【死劲残月】造成的全伤害提高，并造成流血积蓄值，角色对流血状态的敌人造成的全伤害提高"
   
   
Some song it seems
	"<color=#FFCF47FF>Mysterious Jumping Snake</color>"
	"Curiosity sweet flower ♪"
    "Smart Bunny Man"
    "Fragrant and Yingying Sleeves"
    "Colorful Butterfly in Spring"
    "master chirp chirp chirp"
  		  
 CN Text 
   "<color=#FFCF47FF>神秘跳跳蛇</color>" 
   "好奇甜甜花♪"     
   "灵巧兔兔侠"
   "馥郁盈盈袖"
   "春日彩彩蝶"
   "大师啾啾啾"
   

