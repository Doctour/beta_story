Google translate
	"The inscription is inscribed on it:"
     •
    ""The body of 〇〇 lives here, and his death saved the entire city from the ravages of the Houkai beast—"
     •
    "—by his soul poured into the grave, the earth shall be richer than the sky.""
     •
    ""Her sacrifice kept civilization alive.""
     •
    ""Here is a hero who changed the world; reflect on his mistakes, remember his accomplishments—"
     •
    "—he is mortal too.""
     •
    "The Listless Man:"
     •
    "Man with unsteady steps:"
     •
    "The man who is distracted:"
     •
    "A woman in a daze:"
     •
    "A woman looking up at the sky:"
     •
    "The man staring at the ground:"
     •
    "...?"
     •
    "not long ago--" 
 CN Text 
   "碑文铭刻其上："
    •
   "「此处栖息着〇〇的遗体，他的死亡使一整座城市免于崩坏兽的肆虐——"
    •
   "——正因他的灵魂注入坟墓， 大地将比天空更为丰富。」"
    •
   "「她的牺牲，使文明得以延续。」"
    •
   "「这是一位改变了世界的英雄；反思他的过错，铭记他的功劳——"
    •
   "——他亦是凡人。」"
    •
   "无精打采的男人："
    •
   "脚步不稳的男人："
    •
   "走神的男人："
    •
   "发呆的女人："
    •
   "抬头望天的女人："
    •
   "盯着地面的男人："
    •
   "……？"
    •
   "不久之前——"