Translated again with google
	"Susana walked to the desk in the house and opened the terminal placed there. After entering a series of security keys, a communication interface popped up, followed by a slightly impatient voice ——"
    •
  "What unexpected situation did you encounter this time, daredevil Suzana?"
    •
  "Yes, sorry, Dr. Tesla!"
    •
  "Ah, no, no, I didn't make any mistakes this time! This is a very routine and necessary regular contact!"
    •
  "Dr. Tesla, hello."
    •
  "Oh, Xi'er, have you arrived in Changkong City yet? It's faster than Jiwotou and I expected. It seems that the breakout operation with the reloaded bunny went well."
    •
  "Well, the rabbit's performance in vehicle form is very good, thanks to it, I was able to break through the blockade of "The Faceless". "
    •
  "I've seen the previous combat records. I didn't expect that the "Faceless One" could even get the RPC series of gunships. "
    •
  "Hmph, after all, it's bandits who occupy mountains and become kings. They really don't have any tactical wisdom at all to put that kind of large-scale armed forces in narrow tunnels."
    •
  "However, in order to deal with that thing, the energy consumption of reloading the bunny should not be small."
    •
  "Well. In fact, after coming out of the tunnel, the little rabbit enters a dormant state and cannot change its structure anymore."
    •
  "This is also the biggest risk of this battle. Although you, as the "original owner" of the heavy equipment bunny, can use most of her functions, you cannot replenish her energy. "
    •
  "After all, it has been fighting side by side with the "current owner" for a long time, and has already completed many evolutions and iterations, and has formed a nearly symbiotic relationship with the "current owner" in the energy system. "
    •
  "That seems to be the case."
    •
  "Huh? That is to say, as long as the little rabbit is not returned to the "current owner" Miss Bronya, it will not be able to charge? Then the next battle..."
    •
  "Of course I can't use it anymore. Don't think too much, it's just a one-shot deal—hey, didn't these all synchronize with you when the plan was first made?"
    •
  "Ah... yes, yes! Of course I remember! Just to make sure, hehe~"
    •
  "However, since the use of bunnies is so restrictive, why didn't the headquarters send Miss Bronya to participate in the battle in person?"
    •
  "Sister Bronya seems to be performing a very important task recently, so she cannot act with us."
    •
  "A very important task... huh?"
    •
  "..."
    •
  "On the communication screen, the expressions of the two doctors suddenly became a little ambiguous."
    •
  "As expected of Miss Bronya! It must be a much more urgent task than ours, right? But so..."
    •
  "Xie'er, don't you know what her mission is?"
    •
  "This...Although I asked at the time, Sister Bronya refused to say it, she just looked confident and asked me to "look forward to it". "
    •
  "She also said something like "It will definitely subvert the perception of the industry" and "A masterpiece that has never been witnessed", but she spoke too fast at the time, and I couldn't hear the complete sentence. "
    •
  "Ahem... I'm afraid we have some clues about this matter."
    •
  "Really? Doctor, what did Sister Bronya do?"
    •
  "This has to start with the "Destiny Reorganization Plan". You should have heard of it to some extent, right? "
    •
  "Well, I heard from Yarvette and the others that it is said that the main content is to discuss the merger of the two major organizations, Destiny and Anti-Entropy?"
    •
  "That's right. Anti-Entropy used to be an independent organization from Mandate of Heaven, but now there is no ideological conflict between Anti-Entropy and Mandate of Heaven."
    •
  "Since there is no reason to act independently, it is more efficient for the two organizations to merge into one for the common purpose of today."
    •
  "Dr. Tesla also seemed impatient to "teach the people at Changguang Research Institute how to make weapons". "
    •
  "Heh, who told their boss to be a hands-off shopkeeper, always letting his subordinates make a bunch of tasteless things."
    •
  "So, is sister Bronya going to help promote the organization merger? With Teresa and the others?"
    •
  "That's not... her matter, it's actually related to another problem that arose after the merger of the organization."
    •
  "In short, it is the matter of "Who will be the highest person in charge of the New Mandate of Heaven". "
    •
  "Well, let me think about it...Since it is the merger of Destiny and Anti-Entropy, it stands to reason that the Bishop of Destiny or the Anti-Entropy Leader should be in charge, right?"
    •
  "I remember that Mr. Walter was the "temporary leader" of the two organizations before..."
    •
  "That's right. Whether Bishop Teresa or Walter Yang will be the leader of the new organization, this matter should have drawn a conclusion based on actual work needs, but..."
    •
  "But that guy, Walter, suddenly ran over and told us that he would resign from the position of leader and concentrate on animation!"
    •
  "Huh? Are you doing...animation?"
    •
  "Ah, I know, I know! I watched the animation of "Sword of Magic" when I was a child, and it was directed by Mr. Walter himself!"
    •
  "...Indeed, Anti-Entropy has been involved in related industries before in order to raise funds."
    •
  "But this time, Joachim seems to want to devote himself to animation production, and he has even changed his position on the anti-entropy internal communication software to "original painting designer"..."
    •
  "Mr. Walter, what a strong mobility!"
    •
  "Don't praise him!"
    •
  "Yes, I'm sorry!"
    •
  "Ah, could it be that sister Bronya..."
    •
  "It seems that you have also thought of it. Bronya has always been very interested in making games, and Anti-Entropy's animation company is also considering expanding its video game business..."
    •
  "So, as you can imagine, now Bronya has started her internship as a research and development engineer by Joachim's side."
    •
  "I see. The phrase "Stay tuned" originally refers to the game works..."
    •
  "Well, it really is sister Bronya."
    •
  "But... wait a minute, if Mr. Walter gives up being the leader of the new organization, that means..."
    •
  "That's right, Teresa will directly become the supreme person in charge, that is, the "bishop" of the new destiny. Seriously, such a corny title...don't you guys consider changing your name? "
    •
  "Well……"
    •
  "(Bishop Teresa is indeed a very easy-going, unassuming person.)"
    •
  "(However... to govern two organizations at once, will she give up on herself because of too much pressure or too many trivial matters? Or... because of the concentration of power in one body, she becomes obsessed with power and tries to become the hegemon of the world?)"
    •
  "(Not good, the dragon slayer is going to become a dragon!)"
    •
  "Susanna, are you alright?"
    •
  "Huh? Huh? What?"
    •
  "You look up at the ceiling and haven't spoken for several minutes..."
    •
  "It's nothing, nothing, I'm not thinking about Bishop Teresa becoming a villain at all!"
    •
  "You already said it..."
    •
  "We seem to have strayed a bit, let's go back to the current incident in Changkong City."
    •
  "About two weeks ago, our observers in Changkong City reported an incident - an attack on an abandoned chemical plant, thankfully there were no casualties."
    •
  "In the two weeks since then, several similar incidents have occurred one after another. In addition to factories, company buildings, warehouses and even residential areas have all been attacked by force."
    •
  "A week after the initial incident, a group calling itself "The Faceless" claimed responsibility for the attacks through the Internet and leaflets left at the scene. "
    •
  "They declared that "The Faceless" is an organization with hundreds of members and possesses a large number of cutting-edge mecha weapons and other ammunition, and will block the entire city of Changkong City within a week. "
    •
  "As for the reasons for the attack and the lockdown of the city, they did not say anything."
    •
  "Changkong City is still in ruins after all, and the temporary police force alone can't compete with gangsters of this scale—so, we can only rely on us."
    •
  "Susana, who is in charge of the preliminary investigation, and Seele, who is the main combat force, your task is to find out the purpose, composition of the "Faceless People" and the location of their lair. "
    •
  "If the situation permits, of course you can also directly eliminate this organization. In short, the above is the general content of this mission, just in case we repeat it again, there should be no problem, right?"
    •
  "no problem!"
    •
  "Um."
    •
  "Then, from now on, please conduct investigations in the city at your own pace. Dr. Tesla and I will also assist you as intelligence consultants, in addition—"
    •
  "Simultaneously, the notification tone for accepting the message came from the communicators of Seele and Suzana."
    •
  "Here we have found a very suitable "guide" for you, let's meet her first. When it comes to understanding the city - no one should know better than her. "
CN Text
 "苏莎娜走到屋中的书桌旁，打开了摆放在那里的终端。在输入了一连串的安全密钥后，一个通讯界面跃然而出，随之响起的是一个略显不耐的声音——"
   • 
 "这次又遇到什么突发状况了，冒失鬼苏莎娜？"
   • 
 "对、对不起，特斯拉博士！"
   • 
 "啊，不对不对，这次我可没有捅什么娄子！这是非常常规和必要的定期联络！"
   • 
 "特斯拉博士，你好。"
   • 
 "哦，希儿，你已经到达长空市了吗？比我和鸡窝头预计的时间还要快，看来利用重装小兔进行的突围作战很顺利啊。"
   • 
 "嗯，小兔在载具形态的性能非常优秀，多亏了它，我才能突破「无貌者」的封锁。"
   • 
 "我看过之前的作战记录了。没想到「无貌者」连RPC系列的武装直升机都能搞到手。"
   • 
 "哼，终究是群占山为王的匪徒，居然把那种大型武装放到狭窄的隧道里，还真是一点战术智慧都没有呢。"
   • 
 "不过，为了对付那东西，重装小兔的能量损耗应该也不小。"
   • 
 "嗯。其实从隧道里出来之后，小兔就进入休眠状态，无法再变换构造了。"
   • 
 "这本来也是这次作战最大的风险。虽然你作为重装小兔的「原主人」，能够使用她的大部分机能，但却无法为她补充能源。"
   • 
 "毕竟它一直以来都在和「现主人」并肩作战，早已完成了多次的进化和迭代，在能源系统上也和「现主人」形成了近乎共生的关系。"
   • 
 "好像确实是这样呢。"
   • 
 "咦？那也就是说，只要不把小兔还给「现主人」布洛妮娅小姐，它就没法充电了？那接下来的作战……"
   • 
 "当然也就没法再使用它了。别想太多，一锤子买卖而已——喂，这些不都是一开始制定计划的时候就跟你同步过了吗？"
   • 
 "啊……是、是的！我当然记得！只是确认一下，嘿嘿~"
   • 
 "不过既然使用小兔有这么大的限制，为什么总部不派布洛妮娅小姐亲自参加作战呢？"
   • 
 "布洛妮娅姐姐最近似乎在执行一项非常重要的任务，所以没法和我们一起行动。"
   • 
 "非常重要的任务……么。"
   • 
 "……"
   • 
 "通讯屏上，两位博士的神色忽然变得有些意味难明。"
   • 
 "不愧是布洛妮娅小姐！肯定是比我们这里要紧急很多的任务吧？不过这么说来……"
   • 
 "希儿，你难道也不知道她的任务是什么？"
   • 
 "这个……虽然我当时问了，但是布洛妮娅姐姐不肯说，只是一副成竹在胸的样子，让我「好好期待一下」。"
   • 
 "她还说了什么「一定能颠覆业界的认知」、「未曾见证过的大师之作」之类的话，但她当时语速太快，我没有听清完整的句子。"
   • 
 "咳咳……关于这件事，恐怕我们有一些头绪。"
   • 
 "真的吗？博士，布洛妮娅姐姐她到底去做什么了？"
   • 
 "这还要从「天命重组方案」开始说起。你们应该多少有些耳闻吧？"
   • 
 "嗯，我听亚尔薇特她们说过，据说主要的内容是商讨天命和逆熵两大组织的合并？"
   • 
 "没错。逆熵过去是从天命独立出来的组织，但是现在的逆熵和天命之间已经不存在理念上的冲突。"
   • 
 "既然没有了各自为政的理由，那么为了如今的共同目的，两大组织还是合而为一更加有效率。"
   • 
 "特斯拉博士似乎也迫不及待要「教长光研究所的人怎么做武器」了。"
   • 
 "呵，谁叫他们老板是个甩手掌柜，总是放任手下做一堆没有品味的东西。"
   • 
 "所以，布洛妮娅姐姐是去帮忙推进组织合并的事情了吗？和德丽莎她们一起？"
   • 
 "那倒也不是……她的事情，其实是跟组织合并后产生的另一个问题有关。"
   • 
 "简而言之，就是「新天命的最高负责人由谁来担任」这件事。"
   • 
 "唔，我想想啊……既然是天命和逆熵合并，那么按理来说，应该是由天命主教或者逆熵盟主来担任负责人吧？"
   • 
 "我记得，之前瓦尔特先生还当过两个组织的「临时盟主」……"
   • 
 "没错。究竟是由德丽莎主教还是瓦尔特·杨来担任新组织的领袖，这件事本来应该根据实际工作的需要得出一个结论，可是……"
   • 
 "可是瓦尔特那家伙，却突然跑过来跟我们说要辞去盟主的职位，专心去做动画！"
   • 
 "诶？做……动画吗？"
   • 
 "啊，我知道我知道！我小时候就看过《魔剑神机》的动画，那是瓦尔特先生亲自执导的！"
   • 
 "……确实，逆熵为了筹措资金，以前涉足过相关的产业。"
   • 
 "但这次，约阿希姆似乎是想要全身心投入到动画制作中，他甚至已经将自己在逆熵内部通讯软件上的职位改为了「原画设计师」……"
   • 
 "瓦尔特先生，好强的行动力！"
   • 
 "别夸他！"
   • 
 "对、对不起！"
   • 
 "啊，难道说，布洛妮娅姐姐她……"
   • 
 "看来你也想到了。布洛妮娅一直对制作游戏有非常浓厚的兴趣，而逆熵的动画公司也在考虑拓展旗下的电子游戏业务……"
   • 
 "所以，如你所想，现在布洛妮娅已经在约阿希姆的身边，作为研发工程师开始了实习工作。"
   • 
 "原来如此。那句「敬请期待」，原来是指游戏作品啊……"
   • 
 "嗯，果然是布洛妮娅姐姐呢。"
   • 
 "不过……等一下，如果瓦尔特先生放弃当新组织的领袖，那也就是说……"
   • 
 "没错，德丽莎会直接成为最高负责人，也就是新天命的「主教」。说真的，这么老土的头衔……你们不考虑换个名字吗？"
   • 
 "唔……"
   • 
 "（德丽莎主教确实是非常随和、没什么架子的人。）"
   • 
 "（不过……一下要治理两个组织，她会不会因为压力太大或者琐事太多而自暴自弃？又或者……因为集权力于一身，开始醉心于权势，企图成为世界的霸主？）"
   • 
 "（不好，屠龙者要变成恶龙啦！）"
   • 
 "苏莎娜，你没事吧？"
   • 
 "啊？嗯？什么？"
   • 
 "你抬头看着天花板，已经好几分钟没说话了……"
   • 
 "没什么没什么，我完全没有胡思乱想德丽莎主教变成大反派的事！"
   • 
 "你已经说出来了……"
   • 
 "我们似乎有些跑题了，还是回到眼下长空市的事件上来吧。"
   • 
 "大约两周前，我们在长空市的观察员报告了一则事件——一间废弃化工厂遭到袭击，所幸没有人员伤亡。"
   • 
 "而自那之后的两周时间内，又陆陆续续发生了数起类似事件。除了工厂之外，还有公司大楼、仓库甚至居民区，都遭到了武力袭击。"
   • 
 "最初的事件发生一周后，有一个自称「无貌者」的组织通过网络和留在现场的传单，声称为这些袭击事件负责。"
   • 
 "他们宣称「无貌者」是一个拥有数百名成员、并持有大量尖端机甲武器和其他军火的组织，并将在一周内对长空市进行全城封锁。"
   • 
 "至于进行袭击和封锁城市的原因，他们一概没有说明。"
   • 
 "长空市毕竟还是一片废墟，只靠临时警力，根本无法和这种规模的匪徒抗衡——所以，只有靠我们出马了。"
   • 
 "负责前期调查的苏莎娜、作为主要战力的希儿，你们的任务就是查清楚「无貌者」的目的、成员构成以及他们老巢的位置。"
   • 
 "如果情况允许，你们当然也可以直接清除掉这个组织。总之，以上就是这次任务的大致内容，以防万一我们又重复了一遍，应该没有问题吧？"
   • 
 "没有问题！"
   • 
 "嗯。"
   • 
 "那么，接下来就请你们按自己的步调在市内进行调查吧。我和特斯拉博士也会作为情报顾问协助你们，除此之外——"
   • 
 "希儿和苏莎娜的通讯器上同时传来接受信息的提示音。"
   • 
 "这边为你们物色了一位非常合适的「向导」，首先去和她碰头吧。论起对这座城市的了解——应该没有人比她更有心得。"