Google translate
	"It seems that there is finally a gap in their attack—"
     •
    "—Susanna, prepare to retreat!"
     •
    "Retreat, retreat? To where?"
     •
    "The two Xi'er looked at the portal behind them at the same time, and nodded to each other."
     •
    "Remember, Xier said before that she felt a "relatively stable area" in this sea of quantum - that should be the world behind this portal. "
     •
    "And the gray snake's body will drift here, perhaps it was also affected by the natural flow of the Quantum Sea. Or..."
     •
    "He discovered some secrets of that world, and was silenced because of it."
     •
    "Oh? Isn't that dangerous too?"
     •
    "However, we are now surrounded by a steady stream of quantum creations. If we continue to fight, it may be difficult to survive until the day we are found by the rescue forces."
     •
    "In this situation... Even if you go through this door, you will go to a different world with completely different rules from reality——it is an adventure worth trying for us."
     •
    "Of course, there is a more fortunate situation - that is, we are directly rejected by the rules of that world."
     •
    "Luckier? Isn't it bad to be rejected?"
     •
    "If a person is rejected by the rules of the Quantum Sea, then he will immediately return to the Essence World—I thought you knew how the "Genius" of Destiny works? "
     •
    "Ah...is that so?"
     •
    "Wait—that is, as long as we let the rules of Quantum Sea actively negate ourselves, we can return to Changkong City?"
     •
    "Well. To be honest, this guy has encountered a similar situation before—it's just that she was not satisfied with the outcome of the matter at that time, so she returned to the Quantum Sea again and rescued her most important partner."
     •
    "—Perhaps because of some subtle emotions, she didn't directly name "Bronya". "
     •
    "However... the situation Suzanne mentioned is difficult to happen naturally."
     •
    "We... have to be mentally prepared to be forced to live in a different world for a period of time."
     •
    "But what Xier means is that this is at least better than being blocked in a "corner" like now and repeatedly under the enemy's siege? "
     •
    "Well, that's how we see it. Where's Suzanne?"
     •
    "You are experts in Quantum Sea! Of course I listen to you."
     •
    "Okay, Suzana is the first to go, Seele is the second, and I am the last. Those quantum creations are coming together soon, let's hurry up."
     •
    "Speaking, she pushed the B-rank Valkyrie directly. And the moment Suzana's hand touched the portal—"
     •
    "A beam of soft white light envelops her, like the wings of an angel, or the bloom of a white flower."
     •
    "It seems that her luck is indeed worthy of the name—the flood of white light is indeed the first sign that the sea of quantum will push people back to the original world."
     •
    "Heh, it's good to be lucky. In this way, we don't have to rush to the world over there for adventure?"
     •
    "Well, if you can wait until the fate of the rescue—"
     •
    "Before she finished speaking, the endless darkness suddenly covered like a curtain, and then there was a whirl of heaven and earth."
     •
    "The gravity abnormality that seemed to tear the body apart followed, but Xier couldn't feel the pain."
     •
    "She quickly realized that when the two "triangles of stability" that she and Suzana formed together loosened—"
     •
    "It's not just Suzanne who will be affected."
     •
    "The originally stable portal is obviously undergoing a violent expansion... and the direct result of this incident is that the two Xier who are currently separated—"
     •
    "Introduced into the world bubble with different physical parameters."
     •
    "(Seele...!)"
     •
    "In this sudden change, what the girl can do..."
     •
    "...seems to be nothing but prayers."
 CN Text 
   "看来，它们的进攻终于有了一丝空隙——"
    •
   "——苏莎娜，准备撤退！"
    •
   "撤、撤退吗？向哪里？"
    •
   "两位希儿不约而同地看向了她们身后的那道传送门，又互相点了点头。"
    •
   "还记得吗，之前希儿说，她在这片量子之海中感受到了一处「相对稳固的区域」——那应该就是这个传送门背后的世界。"
    •
   "而灰蛇的尸体会漂流到此处，或许也是收到了量子之海自然流向的影响。又或者……"
    •
   "他发现了一些那个世界的秘密，并因此被人灭口。"
    •
   "唉？那不是也很危险吗？"
    •
   "可是，我们现在被源源不断的量子造物包围在了这里，如果一直战斗下去，可能很难支撑到被救援部队找到的那一天。"
    •
   "在这种状况下……即使穿过这道门，会前往一个规则与现实完全不同的异世界——对我们来说也是一种值得尝试的冒险。"
    •
   "当然，还有一种更幸运的情况——就是我们被那个世界的规则直接拒绝。"
    •
   "更幸运？被拒绝难道不是很糟糕吗？"
    •
   "如果一个人被量子之海的规则拒绝，那么他会立即返回本征世界——我以为你知道天命的「格尼乌斯」是怎样运作的？"
    •
   "啊……是这样吗。"
    •
   "等等——也就是说，只要我们让量子之海的规则主动否定我们自己，我们就能返回长空市了？"
    •
   "嗯。说实话，这家伙之前就碰到过一次类似的情况——只是那时她并不满意事情的结局，因此再一次返回了量子之海，救出了自己最重要的伙伴。"
    •
   "——或许是因为一些微妙的情感，她并没有直接点出「布洛妮娅」这个名字。"
    •
   "不过……苏莎娜所说的那种情况很难自然出现。"
    •
   "我们……必须要有在异世界被迫生活一段时间的心理准备。"
    •
   "但希儿的意思是，这至少好过像现在这样，被堵在「墙角」反复承受敌人的围攻？"
    •
   "嗯，我们是这么看的。苏莎娜呢？"
    •
   "你们可是量子之海的专家啊！我当然听你们啦。"
    •
   "那好，苏莎娜第一个走，希儿第二，我最后。那些量子造物又快聚集过来了，我们抓紧时间。"
    •
   "说着，她直接推了这名B级女武神一把。而就在苏莎娜的手接触到传送门的一瞬间——"
    •
   "一束轻柔地白光将她包裹在内，似是天使的羽翼，又像白花的绽放。"
    •
   "看来，她的幸运的确名副其实——那充盈的白光，的确是量子之海将人排回本征世界的初兆。"
    •
   "呵，走运的人就是好啊。这样一来，我们到也不用急着去那边的世界泡冒险了？"
    •
   "嗯，如果能等到天命的救——"
    •
   "她的话未说完，无尽的黑暗就突然如幕布般遮盖下来，然后便是一阵天旋地转。"
    •
   "仿佛要将身体撕裂般的重力异常随之而来，但希儿却感受不到疼痛。"
    •
   "她迅速地意识到，当两个自己和苏莎娜一起构成的「稳定三角」出现松动时——"
    •
   "会受到影响的，其实绝不仅仅只是苏莎娜自己。"
    •
   "那道原本稳定的传送门，显然正在进行一场剧烈的扩张……而这件事的直接结果，就是将正处于分离状态的两个希儿——"
    •
   "以不同的物理参数分别传入了世界泡当中。"
    •
   "（希儿……！）"
    •
   "在这场突如其来的变故中，少女能做的……"
    •
   "……似乎唯有祈愿。"