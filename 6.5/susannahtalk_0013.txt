Google translate
	"(this is……)"
     •
    "On the desk in the corner, there seem to be some scribbled notes."
     •
    "On x, x, two boxes of "Kang", for sale. Need to be frozen. "
     •
    "On x, x, "new", five boxes, for sale. The branch building in the suburbs seems to have been attacked, and they should keep a low profile recently. It would be too wasteful to use the goods on themselves. "
     •
    "On x month x day, "Shou", a box, for sale. The road out of town is blocked, damn it..."
     •
    "On xx, xx, the warehouse of "Kang" was also attacked, and finally was robbed. There are still a lot of goods that I haven't had time to move..."
     •
    "(Looks like the owner here is sneakily moving some kind of cargo.)"
     •
    "(Just what are "Kang", "Xin" and "Shou"?)"
 CN Text 
   "（这个是……）"
    •
   "在角落的书桌上，似乎摆放着一些潦草的笔记。"
    •
   "x月x日，「康」，两箱，待售。需冷冻。"
    •
   "x月x日，「新」，五箱，待售。郊区的分部大楼似乎被袭击了，最近应该低调行事，要是把货用在自己身上就太浪费了。"
    •
   "x月x日，「寿」，一箱，待售。出城道路被封锁，该死……"
    •
   "x月x日，「康」的仓库也被袭击了，终于还是被抢走了。没来得及搬走的货还有很多……"
    •
   "（看起来，这里的主人在偷偷摸摸地搬运某种货物。）"
    •
   "（只是这个「康」、「新」和「寿」又是什么？）"