Google translate
	"Sister Xier, and...it should be sister Suzana, hello."
     •
	"Xiao Kong? Why are you here?"
     •
	"Wait, Seele, how does she know my name?"
     •
	"Ah. So... Sora is the "guide" that Dr. Tesla and the others said? "
     •
	"Well, that's it."
     •
	"We haven't seen each other for almost half a year, right? This time it's my turn to help Sister Seele."
     •
	"So that's it. Thank you, Sora."
     •
	"Ahem, er, is this lady..."
     •
	"Ah, sorry Suzanne, I forgot to introduce you."
     •
	"Xieer briefly told Suzana about how she met Xiao Kong."
     •
	"Huh? In other words...you two met on the moon when the "Stigmata Project" was launched? The first encounter was outside the earth... Oh my god, isn't this too dreamy? "
     •
	"—But Sora, since you are our guide, that means, have you had any special experiences here?"
     •
	"Well. In the past, I and other children who were taken in by the teacher lived in this Changkong City."
     •
	"To be more specific, it is living in this shed. The teacher called this "nest", and we are the children in the nest. "
     •
	"Xiao Kong and the others used to do waste picking work nearby in order to subsidize their family. She knows all the streets, alleys, and even hidden roads here."
     •
	"Sister Xier, I'm not that good...you can't trust Lyle's words too much. However, we are indeed familiar with this place, after all, it is our hometown."
     •
  "That's great! The city is deserted, and the southeast and the north are almost the same, but it's hard to find even a person to ask for directions. When I first arrived here, it was almost impossible to move without turning on the navigation."
     •
  "..."
     •
  "Huh? What's the matter, Seele?"
     •
  "I was just wondering...the reports say that the Faceless Men have organized many attacks in Sky City, right? When I first came here, I did encounter a blockade made up of mechs at the edge of the city. "
     •
  "Thinking about it this way, the "Faceless Men" seems to be a violent group with a rather flamboyant style of behavior. "
     •
  "But...why are the streets still so deserted? Although there are indeed not many citizens here, we have not found any traces of suspected activities of members of the "Faceless". "
     •
  "..."
     •
  "Speaking of it this way, this is indeed a bit weird, but the "Faceless Ones" claim to have several hundred members, more than the entire village. "
     •
  "Actually...it's not like that, Sister Xier."
     •
  "Um?"
     •
  "Although the "Faceless Ones" claim to be powerful, they don't usually swagger around. It is said that they will only show up when an attack is launched, complete the attack as quickly as possible, and then withdraw and leave. "
     •
  "So far, no one has actually seen what they look like."
     •
  "That's why it's called "The Faceless One"... No wonder I've searched for days and still found nothing! "
     •
  "That's why. In this way, everyone will be even more afraid to go to the streets."
     •
  "The villains lurking in the shadows are more terrifying than the dangers that can be seen with the eyes. After all, no one knows when they will show up."
     •
  "Oh, then, if you say that, shouldn't I have passed them by when I sneaked in to investigate?"
     •
  "But since they didn't attack me...it means my disguise was relatively successful, right?"
     •
  "this……"
     •
  "However, the reconstruction work in Changkong City is progressing very slowly. There are not many permanent residents here."
     •
  "When the Great Crash just ended, there were indeed many people who gathered here from all directions. Some were original citizens who wanted to rebuild their homes, some were businessmen who saw opportunities, and some just came to try their luck."
     •
  "But as time goes on, there are fewer and fewer people who can stick around here."
     •
  "Eh? Is it because there are better opportunities elsewhere?"
     •
  "Well, you can indeed say that. After all, Changkong City has been abandoned for too long. Everything needs to be built from scratch, and this requires a lot of manpower, material resources and financial resources."
     •
  "Moreover, it may be because the collapse of Changkong City happened earlier, and everyone is used to its dilapidated appearance, so after the big collapse, they did not pay enough attention here..."
     •
  "..."
     •
  "However, looking at the bright side, Honkai will indeed no longer threaten this place. Although the destiny is also in a state of being overwhelmed, as long as you realize the problem, everyone will work hard to come up with a solution."
     •
  "Also, guiding people in times like these is exactly what we have to do in our work."
     •
  "..."
     •
  "Xie'er, you're so good! Of course, I will do my best!"
     •
  "Ah... Sister Seele, Sister Suzana, thank you."
     •
  "Even now, there are still people who insist on staying in Changkong City and insisting on treating this place as their hometown. I think that one day, Changkong City will become a city that everyone wants to live in again!"
     •
  "ah--"
     •
  "Susanna?"
     •
  "Xie'er, this child... this child... I like her so much!"
     •
  "Susana rushed to Xiao Kong's side and hugged her tightly."
     •
  "Su, Sister Susanna!?"
     •
  "Susanna put her arm around Xiaokong, and raised her fist high with the other hand."
     •
  "Listen up, Suzanne! As the Valkyries, we must take down the Faceless!" "
     •
  "But... I'm not a Valkyrie..."
     •
  "It doesn't matter! Let's go to investigate the location where the "Faceless One" appeared now!" Give them a raid too! "
     •
  "Ah... So, they did show up nearby last night..."
     •
  "Hey, then I'm counting on you, "Guide"! "
CN Text
   "希儿姐姐，还有……应该是苏莎娜姐姐吧，你们好。"
    • 
  "小空？你怎么在这里？"
    • 
 "等等，希儿，她怎么知道我的名字？"
    • 
 "啊。所以……小空就是特斯拉博士她们说的「向导」？"
    • 
 "嗯，就是这样啦。"
    • 
 "我们差不多有半年没见了吧？这次就轮到我来帮助希儿姐姐啦。"
    • 
 "原来是这样。谢谢你，小空。"
    • 
 "咳咳，那个，这位小姐到底是……"
    • 
 "啊，抱歉，苏莎娜，我都忘了跟你介绍。"
    • 
 "希儿将与小空结识的经过，简略地向苏莎娜诉说了一遍。"
    • 
 "诶？也就是说……你们是在「圣痕计划」启动的时候，在月球上认识的？第一次相遇是在地球之外……天啊，这也太梦幻了吧？"
    • 
 "——不过，小空，既然你是我们的向导，也就是说，你在这里也有过什么特殊的经历吗？"
    • 
 "嗯。过去，我和其他被老师收留的孩子们，都生活在这座长空市。"
    • 
 "更具体地说，就是生活在这片棚区。老师把这里称为「巢」，我们便是巢里的孩子。"
    • 
 "小空他们为了补贴家用，都曾经在附近做过拾荒的工作。这里的街道巷弄，乃至暗路密道，她都了如指掌。"
    • 
 "希儿姐姐，我没有那么厉害啦……你可不能太相信莱尔说的话。不过，我们的确很熟悉这里，毕竟是自己的家乡嘛。"
    • 
 "那太好了！这座城市冷冷清清的，东南西北都差不多，却连个问路的人都难找。我刚到这里的时候，不开导航简直就寸步难行。"
    • 
 "……"
    • 
 "嗯？怎么了，希儿？"
    • 
 "我只是在想……报告上说，「无貌者」在长空市组织了许多次袭击活动，对吧？我刚刚来到这里的时候，也确实在市区边缘遭遇了由机甲组成的封锁线。"
    • 
 "这样想来，「无貌者」似乎是一个行事风格相当张扬的暴力团体。"
    • 
 "但……为什么街道上还是这么冷清呢？虽然这里的确没有多少市民，我们却也没有发现任何疑似「无貌者」成员活动的痕迹。"
    • 
 "……"
    • 
 "这样说起来，这确实有点古怪呢，「无貌者」可是号称有几百个成员，比一个村的人都还多呢。"
    • 
 "其实……不是那样的，希儿姐姐。"
    • 
 "嗯？"
    • 
 "「无貌者」虽然自称势力庞大，但他们并不在平常招摇过市。据说，他们只会在发起袭击的时候现身，并用最快的速度完成袭击，然后抽身离去。"
    • 
 "到目前为止，还没有谁真正见过他们究竟长什么样子。"
    • 
 "所以才叫「无貌者」啊……怪不得我调查了好几天还是一无所获！"
    • 
 "原来如此。这样一来，大家就更不敢上街了。"
    • 
 "比起双眼能看到的危险，潜藏在阴影里的恶徒要更加恐怖，毕竟谁也不知道他们什么时候就会现身。"
    • 
 "哎呀，那这么说的话，该不会潜入侦查的时候，我就已经和他们擦肩而过了？"
    • 
 "不过既然他们没有对我出手……说明我的伪装还是比较成功的吧？"
    • 
 "这……"
    • 
 "不过，长空市的重建工作进展得很缓慢，这里本来就没有几户常住人家。"
    • 
 "大崩坏刚刚结束的时候，确实一度有很多人，从四面八方聚到这里来。有的是想要重建家园的原市民，有的是窥见了机遇的商人，有的只是来碰碰运气。"
    • 
 "但随着时间的推移，能坚持留在这里的人就变得越来越少了。"
    • 
 "诶？是因为其他地方的机会更好吗？"
    • 
 "嗯，确实可以这么说。毕竟长空市荒废的时间太长了，所有的一切都需要从零开始搭建，而这需要非常多的人力、物力和财力。"
    • 
 "而且，可能是因为长空市的崩坏发生得比较早，大家都习惯了它破破烂烂的样子，所以在大崩坏之后，也没有给予这里足够的关注……"
    • 
 "……"
    • 
 "不过，从好的方面想，崩坏的确不会再威胁这里了。虽然现在的天命也处在应接不暇的状态，但只要意识到问题，大家一定会努力给出应对的方案。"
    • 
 "而且，在这种时刻引导大家，也正是我们在工作中必须担负的责任啊。"
    • 
 "……"
    • 
 "希儿你说得太好了！我、我当然也会尽一份力的！"
    • 
 "啊……希儿姐姐，苏莎娜姐姐，谢谢你们。"
    • 
 "就算是现在，也还是有人坚持留在长空市，坚持把这里当作自己的家乡。我想，总有一天，长空市一定能重新成为一个大家都愿意来居住的城市！"
    • 
 "啊——"
    • 
 "苏莎娜？"
    • 
 "希儿，这孩子……这孩子……我好喜欢她呀！"
    • 
 "苏莎娜一下子窜到小空身边，将她紧紧抱住。"
    • 
 "苏、苏莎娜姐姐！？"
    • 
 "苏莎娜一手搂着小空，一手高举起拳头。"
    • 
 "听好了，苏莎娜！作为女武神，我们一定要把「无貌者」一网打尽！"
    • 
 "可是……我还不是女武神呀……"
    • 
 "没关系！我们现在就去勘察「无貌者」出现过的地点吧！给他们也来一个突袭！"
    • 
 "啊……这么说来，他们昨晚确实在附近出现过……"
    • 
 "嘿嘿，那就拜托你啦，「向导」！"