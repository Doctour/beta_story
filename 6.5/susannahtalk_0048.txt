Google translate
	"Hey--"
     •
    "Um--"
     •
    "Um... no, the door won't move at all, it won't open at all."
     •
    "Let's find another way, Seele."
     •
    "..."
     •
    "Susanna, do you feel that this door is a bit out of place with the surrounding scenery?"
     •
    "The two looked up at the tall door in front of them and observed carefully."
     •
    "Well, indeed, there seems to be a dark mass on the door? It looks... like a flower?"
     •
    "This door doesn't seem to have a lock or a bolt...why won't it open..."
     •
    "Is this... Could it be the flower that keeps it from being opened?"
     •
    "As soon as Suzana spoke, she immediately laughed at herself."
     •
    "Ahaha, what am I talking about... Even in the world bubble, this kind of imagination is too jumpy, isn't it?"
     •
    "Oh, your intuition is very keen, little girl."
     •
    "Eh? The voice..."
     •
    "Who, who is talking? People in the world bubble? Come out! Don't scare me!"
     •
    "But, I'm definitely here."
     •
    "Susanna opened her eyes wide and looked around, but still didn't see a single figure."
     •
    "Ghosts, ghosts, ghosts? Ghosts? Invisible people?"
     •
    "Really, you're a fool to call you sharp. I'm behind this door."
     •
    "Huh... what, it turns out that you just hid behind the door and waited for people to approach, and then you suddenly spoke to scare them?"
     •
    "Hey—who would be so idle to do such a boring thing!"
     •
    "Then what are you doing behind the door?"
     •
    "Of course waiting for someone to open this nasty door...that's impossible from my side."
     •
    "But it doesn't seem to open from our side either."
     •
    "That's because you didn't use the right method. I heard it all. You said that there is a black mass on the door over there-it is a substance unique to this world bubble that can interfere with space."
     •
    "Fortunately, there is a crystal in the border village that can eliminate this substance, called "light salt". Can you find some bucks? That should open the door. "
     •
    "Wait a minute—what did you just say, 'This world is a bubble'? "
     •
    "What's the problem? I'm also a "wanderer" in the Quantum Ocean. Is it strange to know that this is a world bubble? "
     •
    "The... "Wanderer" of the Quantum Sea? "
     •
    "(Listen, sounds so handsome!)"
     •
    "..."
     •
    "May I ask your name?"
     •
    "ah?"
     •
    "Hmm... It's okay to tell you guys, but let's wait until you open the door!"
     •
    "Think about it, even if I give my name now, you can't see my face—it's kind of weird, isn't it?"
     •
    "Well……"
     •
    "(I always feel...he is a very particular person!)"
     •
    "(But we still have to try our best to ask more things at once.)"
     •
    "Sorry, lady behind the door! Although we would like to help you, we have urgent matters of our own..."
     •
    "That's right, we still have to find our companion, she must not be far away!"
     •
    "...A companion? That's fine, then let me help you first."
     •
    "When did you get separated from your companions?"
     •
    "About ten to twenty minutes ago...we had just entered this world bubble at that time, so the exact time is somewhat difficult to estimate."
     •
    "That would rule out the possibility that she went through this door in the first place - because I have been behind this door for more than an hour, and the door has been closed."
     •
    "And this gate is the only exit to leave the border village, that is to say, your companions must still be somewhere in the village."
     •
    "So that's how it is... This is really important information, Xier!"
     •
    "Yeah! Thank you very much for your help!"
     •
    "Then let's go find her!"
     •
    "Hey, wait. Since you are going to the village, let's find some "light salt" and bring them here. "
     •
    "When you find your companions and want to leave here, you still need to open this door—and the information I provided helped you, didn't it?"
     •
    "Well, you are right. However, we don't know the exact location of these "light salts", nor what they look like. "
     •
    "This is easy to handle. When you get to the village, just pay attention to my "tips". "
     •
    "hint?"
     •
    "You'll understand when you see it. Well, let's go—your companions should be anxious too, shouldn't they?"
     •
    "Well...then we're off!"
 CN Text 
   "嘿——"
    •
   "嗯——"
    •
   "唔……不行，这扇门纹丝不动，根本打不开。"
    •
   "我们还是找找别的路吧，希儿。"
    •
   "……"
    •
   "苏莎娜，你有没有觉得，这扇门跟周围的景色有点格格不入？"
    •
   "两人仰视着眼前高大的门扉，仔细观察。"
    •
   "嗯，确实，门上好像还有一团黑乎乎的东西？看着……像是一朵花？"
    •
   "这扇门似乎没有门锁，也没有门闩……为什么会打不开呢……"
    •
   "这个么……会不会就是那朵花才让它无法被打开？"
    •
   "苏莎娜话刚出口，立刻自嘲般地笑了。"
    •
   "啊哈哈，我在说什么呀……就算是在世界泡里，这种设想也太跳跃了吧？"
    •
   "哎呀，你的直觉倒是很敏锐嘛，小姑娘。"
    •
   "诶？声音……"
    •
   "谁、谁在说话？世界泡里的人？快出来！不要吓我啊！"
    •
   "可是，我明明就在这儿啊。"
    •
   "苏莎娜睁大了眼睛环顾周围，却仍然没有见到一个人影。"
    •
   "鬼、鬼鬼鬼怪？幽灵？隐形人？"
    •
   "真是的，刚夸你敏锐你就犯傻。我是在这扇门的后面啦。"
    •
   "呼……什么啊，原来你是专门躲在门后等人靠近，然后再突然说话吓他们一跳？"
    •
   "喂——谁会闲得没事干，去做这么无聊的事啊！"
    •
   "那你躲在门后干什么？"
    •
   "当然是在等人把这扇讨厌的门打开……从我这边是不可能做到了。"
    •
   "但从我们这边好像也打不开。"
    •
   "那是因为你们没有用对方式。我都听到了，你们说那边的门上有团黑色的东西——那是这个世界泡特有的一种能干扰空间的物质。"
    •
   "不过幸好，边境村里就有一种能消除这种物质的结晶，叫做「光盐」。你们能找来几块吗？那样应该就能把门打开了。"
    •
   "等一下——您刚才的原话是，「这个世界泡」？"
    •
   "有什么问题吗？我好歹也算量子之海中的「漂泊者」，知道这里是一个世界泡很奇怪吗？"
    •
   "量子之海的……「漂泊者」？"
    •
   "（听、听起来好帅气！）"
    •
   "……"
    •
   "能请教一下您的名字吗？"
    •
   "啊？"
    •
   "唔……虽然告诉你们也无妨，不过果然还是等到你们把门打开之后再说吧！"
    •
   "你们想啊，就算我现在报上姓名，可你们却看不到我的样貌——这样反而挺奇怪的，不是吗？"
    •
   "唔……"
    •
   "（总觉得……是个非常讲究的人呢！）"
    •
   "（但我们还是要尽可能的，一次性多问出一些东西。）"
    •
   "抱歉，门后的小姐！虽然我们也想帮您的忙，但是我们自己也有紧急的事情要办……"
    •
   "对对，我们还要找我们的同伴呢，她一定还没走远！"
    •
   "……同伴吗？也好，那就先让我先来帮一帮你们吧。"
    •
   "你们是什么时候和同伴走散的？"
    •
   "大约十到二十分钟前……那时我们刚刚进入这个世界泡，所以精确的时间有些难以估计。"
    •
   "那首先可以排除她通过了这扇门的可能性——因为我在这扇门后已经待了一小时以上，门一直是关着的。"
    •
   "而且这扇大门是离开边境村的唯一出口，也就是说，你们的同伴一定还在村落中的某处。"
    •
   "原来是这样……这还真是个重要的情报啊，希儿！"
    •
   "嗯！非常感谢您的帮助！"
    •
   "那我们就快去找她吧！"
    •
   "哎，等等。既然你们要去村落那边，就顺便找一些「光盐」带过来呗。"
    •
   "等你们找到同伴、想要离开这里的时候，也还是需要打开这扇门——而且，我提供的信息也帮助了你们，不是吗？"
    •
   "嗯，您说得没错。可是，我们既不知道这些「光盐」的具体位置，也不知道它们究竟长什么样子。"
    •
   "这个好办。等你们到了村落里面，注意看我的「提示」就好。"
    •
   "提示？"
    •
   "看到的时候，你们自然就明白了。好了，快去吧——你们的同伴应该也很焦急，不是吗？"
    •
   "嗯……那我们出发了！"