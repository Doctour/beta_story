V2 Starts Here
Google translate
    "Morning. The equipment is checked, and the confidence is ready... Valkyrie Suzana, let's go~"
     •
    "Have lunch together? When I was on a mission last time, I found a delicious restaurant!"
     •
    "Hey, it's finally night, take a good bath and then go to bed... Ah, the mission report was closed before it was saved!"
     •
    "My work is done, and I'm one step closer to becoming an A-rank Valkyrie...probably!"
     •
    "Aren't you asleep yet? I'm reading a novel about Valkyries. It's very interesting. Do you want to read it?"
     •
    "It's getting late, so don't think about so many things. You will become stronger if you take a good rest."
     •
    "Okay, it's finally the weekend, and I'm going to read all the novels I haven't opened in time~"
     •
    "Ah, it's Monday again, there are so many things to do, I can do it, come on, come on..."
     •
    "I always feel like my hair is getting longer. Could it be...is this a sign of getting stronger?"
     •
    "Wooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooow
     •
    "Suddenly I feel dizzy... Ah, it must be because I have stayed up too late lately."
     •
    "Wow~ Is this, is this a special training method for Valkyrie? I, I will work hard!"
     •
    "Did you know? Cloudy days are great for training, neither too hot nor too cold."
     •
    "Are you planning to go out later? Let me check the weather for you... Good! It's sunny!"
     •
    "Listening to the sound of rain, I feel my body becomes very relaxed, and I really want to sleep... Ah, no, no, I can't sleep now."
     •
    "When it snows, the whole world seems to be covered with a thick quilt, which makes people feel at ease."
     •
    "Many fruits are difficult to adapt to the hot desert, but dates can be made sweeter."
     •
    "It's okay, it's okay, for the Valkyrie, I've already gotten used to this temperature... ahhhhh!"
     •
    "Let's take a break first~ I bought a milk tea that is very popular recently, but it won't taste good after a long time."
     •
    "Wow, wow, have today's task been completed so quickly? It's really amazing, as expected of you!"
     •
    "Great, all the goals for this week have been completed. Next, let's relax together!"
     •
    "If you can't become an excellent Valkyrie, you will be taken back to inherit the family business... woo... I can't think about it anymore."
     •
    "My family always wanted me to be a gem merchant, but I still have things I want to do."
     •
    "Although family members sometimes can't understand us, family members are also the strength for us to regain our spirits."
     •
    "I want to be a Valkyrie who can stand alone - and I will make it happen in my own way."
     •
    "Staying up late to work will affect the state of the next day, but if you don't stay up late and work, you can't finish it, um..."
     •
    "Actually, I also doubted whether I was not suitable to be a Valkyrie...but I just thought about it, and I won't give up."
     •
    "Oh, the serialized novel has not been updated, I really want to know the ending of that Valkyrie."
     •
    "When I was young, my family would always ask me to dance in front of everyone... Woohoo, that kind of scene is scary even thinking about it."
     •
    "I haven't seen Lord Sushang for a long time. When we meet next time, bring some cups of milk tea to Lord Sushang."
     •
    "My hometown sent dates, do you want to try it? Just take a bite, and all your troubles will disappear."
     •
    "Hee hee, I have prepared a lot of presents, just waiting for this day. So, happy birthday to you!"
CN Text
   "早呀。装备检查完毕，信心准备就绪……女武神苏莎娜，出发出发~"
    •
   "一起吃午饭吗？上次执行任务的时候，我发现一家很好吃的店！"
    •
   "嘿嘿，终于到晚上啦，好好泡个澡然后睡觉……啊，任务报告还没保存就关闭了！"
    •
   "我的工作都完成啦，距离成为A级女武神又近了一步呢……大概！"
    •
   "你也还没睡吗？我在看一本关于女武神的小说，很有意思，要看嘛？"
    •
   "已经不早了，就不要思考那么多事啦。好好休息才会变得更强哦。"
    •
   "好耶，终于到周末啦，我要把那些没来得及拆封的小说全都看完～"
    •
   "啊，又到周一了，要做的事情好多啊，我可以的，加、加油啊……"
    •
   "总感觉头发好像有些变长了，难道说……这就是变强的征兆？"
    •
   "呜呜呜，好紧张，为什么突然盯着我看……难道是我又搞错什么了吗？"
    •
   "突然感觉头好晕……啊，一定是我最近熬夜太多的缘故吧。"
    •
   "哇啊啊～这、这难道是女武神的特殊训练方式吗？我、我会努力的！"
    •
   "你知道吗？阴天的时候很适合训练哦，既不会太晒，也不会太冷。"
    •
   "你一会打算出门吗？我来帮忙看看天气吧……好耶！是晴天！"
    •
   "听着雨声，就会感觉身体变得很放松，很想睡觉……啊，不行不行，现在还不能睡。"
    •
   "下雪的时候，整个世界好像都盖上了厚厚的被子，让人安心。"
    •
   "很多水果都难以适应炎热的沙漠，但椰枣却可以变得更香甜。"
    •
   "没事没事，对于女武神来说，这个温度我早就适应了……啊、啊嚏！"
    •
   "先休息一下吧～我买了最近很受欢迎的奶茶，时间长了可就不好喝啦。"
    •
   "哇哇哇，今天的任务这么快就完成了吗？真是太厉害啦，不愧是你！"
    •
   "太好啦，本周的目标全部都完成啦。接下来，就一起放松一下吧！"
    •
   "如果不能成为优秀的女武神，就要被捉回去继承家业……呜……不能再想了。"
    •
   "我的家族一直希望我能成为宝石商人，只是，我还有自己想做的事情。"
    •
   "虽然家人有些时候没法理解我们，但家人也是我们重新打起精神的力量呀。"
    •
   "我想成为能够独当一面的女武神——我会用自己的方式把它变成现实。"
    •
   "熬夜工作会影响第二天的状态，但是不熬夜工作又做不完，唔……"
    •
   "其实我也怀疑过自己是不是不适合做女武神……但只是想想而已，我不会放弃。"
    •
   "唉，连载的小说一直没有更新，好想知道那个女武神的结局呀。"
    •
   "小时候，家里人总会叫我当着大家面跳舞……呜呜，那种场面，想想都可怕。"
    •
   "好久都没见到素裳大人了。下次见面时，再给素裳大人带几杯奶茶吧。"
    •
   "老家寄来了椰枣，要尝尝吗？只要吃上一口，什么烦心事都不见啦。"
    •
   "嘻嘻，我准备了很多礼物，就等这一天啦。所以呀，祝你生日快乐！"