Google translate
	"..."
     •
    "..."
     •
    "..."
     •
    "did not return……"
     •
    "..."
     •
    "The girl has been wandering here for a long time."
     •
    "This is not a real physical space, its shape, size and even scenery and decoration can be freely changed according to the owner's preferences."
     •
    "But the masters here have tacitly kept the space as it was when it first appeared - which also means that this small world has never experienced "expansion". "
     •
    "Therefore, the girl quickly went back and forth again and returned to the center of the room."
     •
    "Seele didn't come back."
     •
    "The girl came to a conclusion calmly, not because she was unshakable, but because she still couldn't believe the result."
     •
    "What are you kidding... This is our "heart", to be precise, Seele's "heart". "
     •
    "The place reflected in the heart is still there, but the master of the heart is missing. How can such a thing happen?"
     •
    "She obviously...should have come back here right away..."
     •
    "Xi'er, what are we..."
     •
    "Sheel?"
     •
    "!"
     •
    "The girl turned around abruptly."
     •
    "Are you looking for me?"
     •
    "..."
     •
    "Who are you?"
     •
    "I'm Xi'er, why, don't you know me?"
     •
    "Why are you able to enter this room? Could it be that... Xi'er disappeared, and you are the one who caused trouble?"
     •
    "What are you talking about, Seele, I—"
     •
    "The pitch-black sickle engraved with blood suddenly appeared in the hands of the girl in black, and the curved blade silently reached the girl in white's throat."
     •
    "I ask again - who are you?"
     •
    "The girl in white didn't panic because of the blade on her neck, she gazed peacefully into the eyes of the girl in black."
     •
    "Xi'er, are you angry?"
     •
    "No...anger is just used to hide deeper emotions. Does the disappearance of the original owner of this room disturb you that much?"
     •
    "..."
     •
    "Ha, I see, you are also a trick played by Yutu, right? No one else can break in here."
     •
    "No. The reason why I am here is because of the permission of the owner of the room."
     •
    "Ah? You bastard, do you really want to die? How could Xier allow you to enter here!"
     •
    "No, the owner of the room I'm talking about is you."
     •
    "……What?"
     •
    "Don't you understand? This is not her heart space, but your own heart."
     •
    "You perceive me, that's why I'm here; you're angry with me, that's why you have weapons in your hands."
     •
    "Stop talking nonsense! This is Seele and I...we..."
     •
    "Not you, just you."
     •
    "..."
     •
    "I see. Are you afraid of drawing this conclusion? Then, let me do it for you."
     •
    "...shut up."
     •
    "You and Seele are no longer "one body" relationship—"
     •
    "...shut up."
     •
    "—You, are two independent "individuals". This does not require the precondition of "being in the quantum sea". "
     •
    "You are free, Seele, and my best wishes to you. "
     •
    "...Shut up!!!"
     •
    "The ominous black fell suddenly, as if cutting apart the space. The figure of the white girl disappeared like embers in a fire."
     •
    "Even so, her voice still came from somewhere—"
     •
    "...I get it, it will take time for you to get used to it."
     •
    "Do you think I'll believe your nonsense? The dead should just shut up obediently."
     •
    "Thank you so much."
     •
    "ah?"
     •
    "Although I am not "dead" at this moment, I am deeply honored to be called "human". This is indeed the highest compliment. "
     •
    "(Tsk...he is as sick as Yutu...this is an illusion created by someone, it's not self-inflicted.)"
     •
    "...However, it is easier to understand the situation by witnessing than talking."
     •
    "Then wake up, return to the "open" world, and use your own "body" to feel this freedom. "
     •
    "Who wants to listen to you? Get out of here, I have work to do here."
     •
    "No, you're bound to wake up. Because..."
     •
    "The Frontier is very cold after all. "
     •
    "Huh? What are you talking about—"
     •
    "—Ah Choo!"
 CN Text 
   "……"
    •
   "……"
    •
   "……"
    •
   "没有回来……"
    •
   "……"
    •
   "少女已经在此处徘徊了许久。"
    •
   "这里并不是真实的物质空间，其形状、大小乃至风景和装饰都可以随着主人的喜好自由变化。"
    •
   "但这里的主人们默契地一直让空间保持着刚刚显现时的样貌——也意味着，这个小小的世界从未经历过「扩张」。"
    •
   "因此，少女很快再次走完了一个往复，回到了房间的中央。"
    •
   "希儿没有回来。"
    •
   "少女平静地得出了结论，并不是因为没有动摇，而是尚且不能相信这个结果。"
    •
   "开什么玩笑……这里是我们的「内心」，准确地说，是希儿的「内心」。"
    •
   "内心映照出的场所还在，心灵的主人却不见踪影，这种事情怎么可能发生？"
    •
   "她明明……应该一下就回到这里的……"
    •
   "希儿，我们究竟……"
    •
   "希儿？"
    •
   "！"
    •
   "少女猛然回过头。"
    •
   "你是在找我吗？"
    •
   "……"
    •
   "你是谁？"
    •
   "我是希儿啊，怎么，你不认识我了吗？"
    •
   "你为什么能进入这个房间？难道说……希儿不见了，也是你在捣鬼吗？"
    •
   "你在说什么啊，希儿，我——"
    •
   "镌有血色的漆黑镰刀骤然显现在黑衣少女手中，弯弧状的利刃悄无声息地抵上白衣女孩的咽喉。"
    •
   "我再问一次——你是谁？"
    •
   "白衣女孩没有因为架在脖颈上的刀刃而慌乱，她平和地凝视着黑衣少女的眼眸。"
    •
   "希儿，你很愤怒么？"
    •
   "不……愤怒只是用来隐藏更深层的情绪。这个房间的原主人的消失，就这么让你感到不安吗？"
    •
   "……"
    •
   "哈，我明白了，你也是羽兔搞出来的把戏吧？其他人才没有办法闯进这里。"
    •
   "不是的。我之所以会在这里，是因为获得了房间主人的许可。"
    •
   "啊？你这家伙，是真的想死吗？希儿怎么可能允许你进入这里！"
    •
   "不，我说的房间主人，是你。"
    •
   "……什么？"
    •
   "你还不明白么？这里并不是她的心灵空间，而是你自己的内心。"
    •
   "你感知到了我，所以我才会在这里出现；你对我感到愤怒，所以你的手中才会出现武器。"
    •
   "少胡说八道了！这里是希儿和我……我们……"
    •
   "不是你们，只有你。"
    •
   "……"
    •
   "原来如此，你是在害怕得出这个结论么。那么，就由我来代劳吧。"
    •
   "……住嘴。"
    •
   "你和希儿早已不是「互为一体」的关系——"
    •
   "……住嘴。"
    •
   "——你们，是两个独立的「个体」。这并不需要「身处量子之海」那样的前提条件。"
    •
   "你自由了，「希儿」，我为你献上诚挚的祝福。"
    •
   "……住嘴！！！"
    •
   "不祥的黑色猛然落下，似乎将空间都斩断割裂。白色女孩的身影顿时如火中残烬般消失了。"
    •
   "然而即便如此，她的声音依旧从不知何处的角落传来——"
    •
   "……我明白了，这对你来说还需要时间来适应。"
    •
   "你以为我会相信你的胡言乱语吗？死人就赶紧乖乖闭嘴吧。"
    •
   "非常感谢。"
    •
   "啊？"
    •
   "虽然我此刻并没有「死」，但是被称为「人」，我深感荣幸。这的确是最高的赞誉。"
    •
   "（啧……和羽兔一样病得不轻……这是谁制造的幻觉，倒是不打自招了。）"
    •
   "……不过，比起对话，还是亲眼见证更容易让你理解当下的状况。"
    •
   "那就醒来吧，回到「开放」的世界中，用你自己的「身躯」去感受这种自由。"
    •
   "谁要听你的？快滚吧，我在这里还有事情要做。"
    •
   "不，你一定会醒过来的。因为……"
    •
   "「边境」毕竟非常寒冷。"
    •
   "啊？你在说什——"
    •
   "——阿嚏！"