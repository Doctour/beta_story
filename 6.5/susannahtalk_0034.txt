Google translate
	"Phew...it's finally over. I didn't expect him to be a stalker."
     •
    "Looks like he's finally used up his stockpile."
     •
    "In the not-so-broad underground laboratory, broken limbs and fragments of machinery are scattered all over the place."
     •
    "Grey Snake sat casually on the railing beside him, not calling for new weapons. He raised his head slightly, as if what happened around him had nothing to do with him."
     •
    "After fighting for a long time, the other members of "The Faceless" didn't come to rescue you. Are you really an important member of the organization? "
     •
    "Ah."
     •
    "Grey Snake chuckled lightly when he heard the words, as if he felt a little proud."
     •
    "Susanna, I don't think there are any other members of Faceless. "
     •
    "Eh?"
     •
    "Since we entered Sky City, we have always fought against armed mechs or bionic robots. Except for him, we have not even seen a living "faceless" member so far. "
     •
    "And during the entire investigation process, we did not inquire about any eyewitness information about the appearance and shape of members of the "Faceless"—except for him. "
     •
    "Also, the purpose of "The Faceless Man" is to raise a virtual prosthesis for Gray Snake, so his status in the organization should be very high. But in these few operations, he went to the front line in person—and alone, with no one to respond. "
     •
    "It's... really unreasonable."
     •
    "So I guess that the entire "Faceless People" organization is actually a suspicious formation set up by this Gray Snake... From the beginning to the end, "Faceless People" is an organization with only you, right? "
     •
    "..."
     •
    "So...whether it's a statement with hundreds of members, mechas everywhere in the city, or actions such as blocking tunnels, these are all..."
     •
    "Well, it's all a disguise to create the lie that "the Faceless Organization really exists". "
     •
    "From the beginning to the end, our enemy is only the Gray Snake in front of us, and this is the only one."
     •
    "..."
     •
    "I originally thought that as long as I fabricated such a seemingly huge organization, I would be able to confuse those who want to track me down and make them unable to see through my real purpose."
     •
    "Unexpectedly, in front of a discerning audience, these are just poor acting skills."
     •
    "Mr. Gray Snake...why are you so obsessed with having a flesh and blood body?"
     •
    "Grey Snake leaned back slightly, his prosthetic eyes looked behind Xier, as if he was staring into the distance through the wall."
     •
    "Since you know my name, you must also know about me... about what kind of existence the "Grey Snake" is, right? "
     •
    "You are mechanical bionics made by the world snake. Although the individual personalities are different, they all have the same material body, and share memory and even consciousness through the "Grey Snake Network". "
     •
    "Well, not too much. However, I think the "gray snakes" you have come into contact with or heard about should all be "integrated", right? "
     •
    ""Complete"? "
     •
    "In other words, no matter what kind of personality or mental age, what gender they think they are or what kind of likes and dislikes they have, these gray snakes have a complete "personality", right? "
     •
    "Could it be that... there are gray snakes that are not like this?"
     •
    "Ha, of course, there is one in front of you. Or, I used to be one of them - the "incomplete type". "
     •
    "Not all gray snakes can give birth to a complete and solid personality when they awaken consciousness. Due to the randomness of the generation algorithm, about 30% of gray snakes only have a crippled mind - we can only live like the simplest robot ."
     •
    "Hey, is there such a thing?"
     •
    "It's normal if you don't know, because we have almost no ability to act independently, and naturally we can't contact the outside world. However, the World Snake didn't give up on us because of this."
     •
    "In a dark underground workshop, we are contained, and we are constantly absorbing memories through the "Gray Snake Network", perfecting or repairing incomplete personalities. "
     •
    "It is said that as long as we take time, we will eventually become "complete" gray snakes. The irony is that..."
     •
    "...the Gray Snake Network itself no longer exists. "
     •
    "That's right. For the "complete" gray snakes, this undoubtedly means freedom and liberation, but for us, it is like a fetus in the womb, the umbilical cord that supports itself is forcibly torn off. "
     •
    "..."
     •
    "Then, Mr. Gray Snake, how did you survive and gain a complete personality?"
     •
    "...Oh, it's just luck."
     •
    "When I woke up...the scene in front of me was no different from the so-called hell."
     •
    "All around are familiar bodies...same hands and feet like mine, same mask like me, same eyes like mine."
     •
    "I realized right away what had happened - the 'network' was gone, my brothers and sisters around me, they lost their chance to wake up forever. "
     •
    "Yes, I was just lucky enough to awaken to full consciousness before the "network" disappeared. "
     •
    "When I crawled along their lifeless shells, I set the path for the rest of my life."
     •
    "I want to find a real body for my soul, a real temple that can hold the heart."
     •
    "I want to stay away from this cold artificial coffin, and never recall the feeling of being dark, cold, and clammy again."
     •
    "That's why I'm so attached to flesh and blood, wise...Miss Seele."
     •
    "..."
     •
    "I sympathize with what has happened to you and respect your tenacity, Mr. Gray Snake."
     •
    "But I don't agree with one point of yours."
     •
    "oh?"
     •
    "Even a mind without flesh and blood, or even a body at all, is a form of life."
     •
    "I disagree with you... seeing it as worthless."
     •
    "... Seele?"
     •
    "..."
     •
    "It seems that you also have your own experience, which makes you believe in something completely opposite to my belief. But it's a pity..."
     •
    "The time has come."
     •
    "?"
     •
    "Honestly, I'd love to hear your story - but it's just not the time."
     •
    "!"
     •
    "Suddenly, the gray snake swayed again, and its head fell down again. Only this time it didn't land on the waist, but rolled directly to the ground."
     •
    "Oops, it's a fake body!"
     •
    "Guess right."
     •
    "The mechanical parts on the ground are put together to form a gray snake?"
     •
    "Catch him!"
     •
    "Well, there will be a time later."
     •
    "The real body of Gray Snake burrowed into an air vent next to him with incomparable vigor, and the sound of an explosion followed."
     •
    "Seele and Suzana rushed forward, only to find that the vent had been tightly blocked by rubble and debris."
     •
    "Quick, let's go the other way!"
     •
    "Well, did you find him?"
     •
    "No... Looks like we let him escape again..."
     •
    "The two looked at each other with helplessness in their eyes. Suddenly, Suzana seemed to think of something—"
     •
    "Ah, I see!"
     •
    "Um?"
     •
    "Xie'er, do you still remember that we blocked Gray Snake in the warehouse yesterday, but he disappeared as if he had evaporated? Could it be... the same trick as before?"
     •
    "You mean... ah!"
     •
    "An empty warehouse, only some messy boxes and mechanical parts on the ground..."
     •
    "Actually, those are Gray Snakes, right? Just like before, he dismantled himself into fragments and pretended that he was already somewhere else!"
     •
    "In other words, he was actually in the warehouse at that time... He patiently waited for us to leave, then returned to his original state as before, and then left the scene leisurely..."
     •
    "That guy, using the same trick, tricked us twice... successfully escaped under our noses twice..."
     •
    "Um……"
     •
    "Susanna and Xi'er looked at each other again, this time, both of them couldn't help showing wry smiles."
     •
    "In order to solve this major case that happened in Changkong City, they still need to run around."
 CN Text 
   "呼……终于结束了。没想到他还挺死缠烂打。"
    •
   "看来，他终于用完了自己的库存。"
    •
   "并不算宽阔的地下实验室中，机械的断肢和零件碎片散落了一地。"
    •
   "灰蛇随意地坐在一旁的栏杆上，不再呼唤新的武装。他微微抬头，似乎周围发生的事和自己全无关系。"
    •
   "打了这么半天，其他「无貌者」的成员居然没来救你。你真的是组织里的重要成员吗？"
    •
   "呵。"
    •
   "灰蛇闻言轻笑了一声，似乎感到有些得意。"
    •
   "苏莎娜，我想根本不存在其他「无貌者」的成员。"
    •
   "诶？"
    •
   "自从我们进入天穹市以来，跟我们交手的永远是武装机甲或者仿生机器人。除了他以外，我们至今连一个活生生的「无貌者」成员都没有见过。"
    •
   "而在整个调查过程中，我们也没有打探到任何关于「无貌者」成员相貌、形体的目击情报——除了他以外。"
    •
   "还有，「无貌者」的目的是为灰蛇筹措拟真义体，那么他在组织中的地位应该很高。但是这几次行动，他都亲自前往第一线——而且单枪匹马，无人接应。"
    •
   "这……确实不合情理。"
    •
   "所以我猜测，整个「无貌者」组织其实是这位灰蛇布下的一个疑阵……从头到尾，「无貌者」就是只有你一个人的组织，对吗？"
    •
   "……"
    •
   "所以……成员有几百个人的宣言也好，城中无处不在的机甲也好，还有封锁隧道之类的行动，这些都是……"
    •
   "嗯，全部都是为了制造「无貌者组织真实存在」这个谎言而做的伪装。"
    •
   "从始至终，我们的敌人都只有眼前这位灰蛇，仅此一人。"
    •
   "……"
    •
   "我原本以为，只要虚构出这么一个貌似庞大的组织，就能扰乱那些想要来追查我的人，让他们看不透我真正的目的。"
    •
   "没想到，在独具慧眼的观众面前，这些都不过是拙劣的演技。"
    •
   "灰蛇先生……你究竟为什么这么执着于拥有血肉之躯？"
    •
   "灰蛇略微仰了仰身体，他的义眼看向希儿身后，似乎在透过墙体凝视着远方。"
    •
   "你们既然知道我的名字，那么也一定对我……对「灰蛇」是种怎样的存在有所了解吧？"
    •
   "你们是由世界蛇制造的机械仿生人，个体的性格虽然不同，但都拥有同样材质的身体，通过「灰蛇网络」共享记忆甚至意识。"
    •
   "嗯，差不太多。不过，我想你们接触过，或者听闻过的「灰蛇」，应该都是「完整型」吧？"
    •
   "「完整型」？"
    •
   "换句话说，无论是什么样的性格或者心理年龄，自认为是什么性别或者有什么样的喜恶，这些灰蛇都拥有完整的「人格」，对吧？"
    •
   "难道说……还有并非如此的灰蛇吗？"
    •
   "哈，当然了，你面前就有一个。或者说，我曾经是其中之一——「残缺型」。"
    •
   "并非所有灰蛇在觉醒意识时，都能诞生出完整而牢固的人格。由于生成算法的随机性，大约30%的灰蛇只拥有残缺的心灵——我们只能像最简单的机器人一样活着。"
    •
   "居、居然有这样的事吗？"
    •
   "你们不知道也很正常，因为我们几乎没有自主行动的能力，自然也无法和外界接触。不过，世界蛇也没有因此就放弃我们。"
    •
   "在某个不见天日的地下工坊中，我们被收容，并不断通过「灰蛇网络」吸纳记忆，完善、或者说修复并不完整的人格。"
    •
   "据说，只要假以时日，我们终究能够成为「完整型」的灰蛇。可讽刺的是……"
    •
   "……「灰蛇网络」本身不复存在了。"
    •
   "没错。对「完整型」的灰蛇们来说，这无疑意味着自由和解放，但对我们来说，却犹如腹中的胎儿，被硬生生扯断了供养自己的脐带。"
    •
   "……"
    •
   "那么，灰蛇先生，你又是怎么存活下来，并且获得完整人格的？"
    •
   "……呵，不过是运气好一些罢了。"
    •
   "当我醒来的时候……眼前的景象，和所谓的地狱没有什么分别。"
    •
   "周围尽是熟悉的躯体……和我相同的手脚，和我相同的面具，和我相同的眼睛。"
    •
   "我当即就意识到发生了什么——「网络」不存在了，我周围的兄弟姐妹，他们永远失去了醒来的机会。"
    •
   "是的，我只是运气好一些，赶在在「网络」消失之前觉醒了完整的意识而已。"
    •
   "当我顺着他们再无生气的躯壳爬行时，我就确定了我后半生要走的路。"
    •
   "我要为我的灵魂找一具真正的肉体，一座真正能承载心灵的神殿。"
    •
   "我要远离这个冰冷的人造棺木，再也不要回想起那种昏暗、彻寒、而又黏湿的恶心感觉。"
    •
   "这就是我对血肉之躯如此执着的原因，聪明的……希儿小姐。"
    •
   "……"
    •
   "我对你的遭遇感到同情，也对你的坚韧表示敬意，灰蛇先生。"
    •
   "但是我无法认同你的一个观点。"
    •
   "哦？"
    •
   "即使没有血肉之躯，甚至根本没有躯体的心灵，那也是生命的一种形式。"
    •
   "我不同意你……将其视为缺乏价值的存在。"
    •
   "……希儿？"
    •
   "……"
    •
   "看来，你也有自己的经历，它使得你坚信着一些和我的信仰截然相反的东西。不过很可惜……"
    •
   "时间已经到了。"
    •
   "？"
    •
   "说实话，我很乐于听一听你的故事——但如今确实不是时候。"
    •
   "！"
    •
   "突然间，灰蛇又是身形一晃，头部再次掉落了下来。只是这次它没有落在腰间，而是直接滚到了地上。"
    •
   "糟了，是假身！"
    •
   "猜对了。"
    •
   "地上的机械零件，拼、拼成了灰蛇？"
    •
   "快抓住他！"
    •
   "那么，后会有期了。"
    •
   "灰蛇的真身无比矫健地钻入了他身旁的一个通风口，而爆炸声也随之传来。"
    •
   "希儿和苏莎娜连忙赶上前去，却发现那个通风口已经被碎石和瓦砾堵得严严实实。"
    •
   "快，我们走其他路过去！"
    •
   "怎么样，有发现他吗？"
    •
   "没有……看来，我们又让他逃走了……"
    •
   "两人对视一眼，眼神中满是无奈。忽然，苏莎娜像是忽然想到了什么——"
    •
   "啊，我知道了！"
    •
   "嗯？"
    •
   "希儿，你还记不记得昨天我们把灰蛇堵在了仓库里，他却像是蒸发了一样消失不见了？会不会……也是用了跟刚才一样的伎俩？"
    •
   "你是说……啊！"
    •
   "空无一人的仓库，只有一些杂乱摆放的箱子和地上的机械零件……"
    •
   "其实那些就是灰蛇吧？他就像刚刚一样，把自己分解成了零零碎碎的小块，装出自己已经身处别处的假象！"
    •
   "也就是说，那个时候其实他就在仓库里……他耐心地等我们离开之后，再像刚才那样恢复成原状，然后悠然离开现场……"
    •
   "那家伙，用同样的伎俩，骗了我们两次……成功在我们眼皮底下逃走了两次……"
    •
   "嗯……"
    •
   "苏莎娜和希儿再次对视，这一次，两人都不由地露出了苦笑。"
    •
   "为了解决这起发生在长空市的大案，她们仍需奔波。"